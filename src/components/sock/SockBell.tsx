import { faBell } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import { useState, useEffect } from "react";
import { useDispatch } from "react-redux";

import { Stomp } from "@stomp/stompjs";
import SockJS from "sockjs-client";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { sockstatus } from "../../_actions/sock_action";
import { hydrogenStatus } from "../../_actions/hydrogen_action";

import {
  chatRoomList,
  enterRoom,
  pushMessage,
} from "../../_actions/sock_action";

const Container = styled.div`
  position: relative;
`;

const Icon = styled.div`
  font-size: 28px;
  position: relative;
`;

const Count = styled.div`
  padding: 4px;
  border-radius: 50%;
  background-color: red;
  position: absolute;
  top: -4px;
  right: -8px;
  color: white;
  font-size: 13px;
`;

const TextBoxWrap = styled.div`
  padding: 20px;
  background-color: #34aaff;
  position: absolute;
  top: 10px;
  right: 50px;
  border-radius: 15px;
  overflow-y: scroll;
  max-height: 500px;
  &::-webkit-scrollbar {
    width: 20px;
  }
  &::-webkit-scrollbar-thumb {
    background-color: white;
    border-radius: 25px;
  }
`;

const Text = styled.div`
  padding: 10px;
  background-color: white;
  border-radius: 5px;
  margin-bottom: 20px;
  width: 300px;
  line-height: 30px;
`;

export let stompClient: any = null;

export const SockBell = () => {
  const [bell, setBell] = useState<boolean>(false);
  const [publicChats, setPublicChats] = useState<string[]>([]);
  const [textBox, setTextBox] = useState(false);
  const [count, setCount] = useState(0);
  const [chatDb, setChatDb] = useState<any[]>([]);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const user: any = useSelector(
    (state: any) => state?.user?.loginSuccess?.data?.result?.email
  );

  useEffect(() => {
    const connect = () => {
      let Sock = new SockJS("http://192.168.0.233:6112/ws");
      stompClient = Stomp.over(Sock);
      stompClient.connect({}, onConnected, onError);
    };
    connect();
  }, []);
  const onConnected = () => {
    stompClient.subscribe("/chatroom/public", onMessageReceived);
    stompClient.subscribe("/personalChat/chat", onChatReceived);
    // stompClient.subscribe("/hydrogen/public", onStausRecived);
    firstCount();
    dispatch(sockstatus("connect"));
  };

  const onStausRecived = (payload: any) => {
    let payloadData = JSON.parse(payload.body);

    if (payloadData.body.code === 1) {
      dispatch(hydrogenStatus(payloadData.body.results));
    }
  };

  const onChatReceived = (payload: any) => {
    let payloadData = JSON.parse(payload.body);
    console.log(payloadData);
    switch (payloadData.stringCode) {
      case "채팅방생성":
        navigate(`/messenger/${payloadData.result}`);
        break;

      case "기존채팅방 입장":
        navigate(`/messenger/${payloadData.result}`);
        break;
      case "채팅방입장":
        dispatch(enterRoom(payloadData));
        break;
      case "채팅목록":
        dispatch(chatRoomList(payloadData));
        break;
      case "채팅입력시":
        dispatch(pushMessage(payloadData));
        break;
    }
  };

  const onMessageReceived = (payload: any) => {
    let payloadData = JSON.parse(payload.body);
    switch (payloadData.code) {
      case 1: //운영자가 메시지보냄
        const message = payloadData.result.message;
        setPublicChats([...publicChats, message]);

        firstCount();

        break;
      case 2:
        if (payloadData.result == null) {
          //종닫음
          firstCount();
        } else {
          //종오픈
          setCount(payloadData.count);
          setChatDb(payloadData.result);
        }
        break;

      case 3: // 처음 카운트 받아옴
        setCount(payloadData?.count);
        break;
    }
  };

  const onError = (err: any) => {
    console.log(err);
  };

  const firstCount = () => {
    let countMessage = {
      receiverName: user,
    };
    stompClient.send("/app/count", {}, JSON.stringify(countMessage)); // here
  };

  const bellClick = () => {
    setTextBox(!textBox);
    let btnToggle = {
      toggle: !bell,
      receiverName: user,
    };

    stompClient.send("/app/bell", {}, JSON.stringify(btnToggle));
    setBell(!bell);
  };

  return (
    <Container>
      <Icon onClick={bellClick}>
        <FontAwesomeIcon icon={faBell} />
        {count !== 0 ? (
          <Count>
            <span>{count}</span>
          </Count>
        ) : (
          ""
        )}
      </Icon>
      {textBox ? (
        <TextBoxWrap>
          {chatDb
            ? chatDb
                .slice(0)
                .reverse()
                .map((db) => <Text>{db.message}</Text>)
            : ""}
        </TextBoxWrap>
      ) : (
        ""
      )}
    </Container>
  );
};
