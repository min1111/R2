import {
  faChevronRight,
  faCommentDots,
  faGear,
  faList,
  faMobileScreenButton,
  faSearch,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import { useEffect, useState } from "react";
import SockJS from "sockjs-client";
import { Stomp } from "@stomp/stompjs";
import { useSelector } from "react-redux";
import { stompClient } from "../sock/SockBell";

const Container = styled.div`
  height: 100vh;
  background-color: #002d7d;
  position: fixed;
  left: 0;
  top: 0;
  z-index: 9999;
  width: 350px;
  height: 100%;
  transition: 1s;
  display: flex;
  flex-direction: column;
  align-items: center;
  /* display: none; */
  color: #eeeeee;
`;

const Logo = styled.div`
  width: 100%;
  height: 80px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 32px;
  font-weight: 900;
  letter-spacing: 0px;
`;
const Photo = styled.div`
  width: 220px;
  height: 220px;
  border-radius: 50%;
  margin-bottom: 25px;
`;
const Name = styled.div`
  font-size: 28px;
  font-weight: 600;
  margin-bottom: 15px;
`;
const Mail = styled.div`
  font-size: 18px;
  font-weight: 300;
  opacity: 0.8;
  margin-bottom: 35px;
`;
const MenuWrap = styled.div`
  width: 100%;
  height: 80px;
  display: flex;
  margin-bottom: 30px;
`;
const Menu = styled.div`
  width: 33.33333%;
  height: 100%;
  border: 1px solid #eee;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  font-size: 32px;
  span {
    margin-top: 5px;
    font-size: 18px;
  }
`;
const Form = styled.form`
  display: flex;
  margin-bottom: 35px;
`;
const Search = styled.input`
  all: unset;
  width: 250px;
  height: 50px;
  background-color: white;
  display: block;
`;
const Button = styled.button`
  all: unset;
  display: block;
  width: 50px;
  height: 50px;
  background-color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  color: black;
  font-size: 24px;
`;
const UserList = styled.div`
  width: 100%;
`;

const UserWrap = styled.div`
  width: 100%;
  height: 80px;
  border: 1px solid #eee;
  display: flex;
  margin-bottom: 10px;
`;
const Status = styled.div`
  display: block;
  width: 10px;
  height: 78px;
  background-color: #23ee6d;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
`;
const UserInfo = styled.div`
  width: 260px;
  padding-left: 15px;
  display: flex;
  align-items: flex-start;
  justify-content: center;
  flex-direction: column;
`;
const UserName = styled.div`
  font-size: 28px;
  font-weight: 600;
  margin-bottom: 8px;
`;
const UserCellPhon = styled.div`
  font-size: 16px;
  span {
    margin-left: 5px;
  }
`;
const Messenger = styled.div`
  width: 78px;
  height: 78px;
  border: 1px solid #eee;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 30px;
`;

const DoorButton = styled.div`
  width: 15px;
  height: 200px;
  background-color: #002d7d;
  position: absolute;
  top: 40%;
  right: -15px;
  border-top-right-radius: 15px;
  border-bottom-right-radius: 15px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  font-size: 24px;
  font-weight: 900;
  transition: 0.5s;
`;

const friendList = [
  {
    id: 1,
    username: "min",
    phone: "01012341234",
    profil: "3.jpg",
    email: "123@123.com",
  },
  {
    id: 2,
    username: "성찬",
    phone: "01012341234",
    profil: "1.jpg",
    email: "1234@123.com",
  },
];
export const MessengerHeader = () => {
  const [open, setOpen] = useState(false);
  const user: any = useSelector(
    (state: any) => state?.user?.loginSuccess?.data?.result
  );

  const createRoom = (email: string) => {
    const chatMessage: { senderName: string; receiverName: string } = {
      senderName: user?.email,
      receiverName: email,
    };
    stompClient.send("/app/firstContactChat", {}, JSON.stringify(chatMessage));
    setOpen(!open);
  };
  return (
    <Container
      style={{
        transform: `${open ? "translateX(0)" : "translateX(-350px)"}`,
      }}
    >
      <Logo>Pacom</Logo>
      <Photo
        style={{
          background: `${
            user?.profil
              ? `url(http://localhost:6112/img/${user?.profil})`
              : "gray"
          } no-repeat center/cover`,
        }}
      ></Photo>
      <Name>{user?.username}</Name>
      <Mail>{user?.email}</Mail>
      <MenuWrap>
        <Menu>
          <FontAwesomeIcon icon={faUser} />
          <span>내정보</span>
        </Menu>
        <Menu>
          <FontAwesomeIcon icon={faList} />
          <span>리포트</span>
        </Menu>
        <Menu>
          <FontAwesomeIcon icon={faGear} />
          <span>환경설정</span>
        </Menu>
      </MenuWrap>
      <Form>
        <Search></Search>
        <Button>
          <FontAwesomeIcon icon={faSearch} />
        </Button>
      </Form>
      <UserList>
        {friendList.map((list) => (
          <UserWrap>
            <Status></Status>
            <UserInfo>
              <UserName>{list?.username}</UserName>
              <UserCellPhon>
                <FontAwesomeIcon icon={faMobileScreenButton} />
                <span>{list?.phone}</span>
              </UserCellPhon>
            </UserInfo>
            <Messenger
              onClick={() => {
                createRoom(list?.email);
              }}
            >
              <FontAwesomeIcon icon={faCommentDots} />
            </Messenger>
          </UserWrap>
        ))}
      </UserList>
      <DoorButton
        onClick={() => {
          setOpen(!open);
        }}
        style={{
          backgroundColor: `${!open ? "white" : "#6E7E9C"}`,
          color: `${!open ? "#6E7E9C" : "white"}`,
        }}
      >
        <FontAwesomeIcon
          style={{
            transform: `${open ? "rotateZ(180deg)" : "rotateZ(0)"}`,
            transition: "0.5s",
          }}
          icon={faChevronRight}
        />
      </DoorButton>
    </Container>
  );
};
