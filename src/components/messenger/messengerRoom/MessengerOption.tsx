import {
  faChevronUp,
  faSearch,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";

const Wrap = styled.div`
  width: 25%;
  height: 100%;
  background-color: WHITE;
`;

const Form = styled.form`
  padding: 20px 25px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const Input = styled.input`
  all: unset;
  display: block;
  width: 80%;
  height: 50px;
  background-color: #eeeeee;
  padding-left: 15px;
  &::placeholder {
    opacity: 0.5;
  }
`;
const Button = styled.button`
  all: unset;
  display: block;
  width: 20%;
  height: 50px;
  background-color: #eeeeee;
  display: flex;
  align-items: center;
  justify-content: center;
  span {
    opacity: 0.5;
  }
`;
const MenuTitle = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 5px 25px;
  margin: 10px 0;
  font-size: 22px;
  color: rgba(0, 0, 0, 0.3);
  border-top: 1px solid rgba(0, 0, 0, 0.2);
  span {
    font-size: 18px;
    color: rgba(0, 0, 0, 0.7);
    font-weight: 500;
  }
`;

const RoomEnterUserWrap = styled.div`
  padding: 5px 25px;
`;
const AddUserWrap = styled.div`
  padding: 5px 0;
  display: flex;
  align-items: center;
`;
const Icon = styled.div`
  width: 30px;
  height: 30px;
  border-radius: 50%;
  background-color: rgba(0, 0, 0, 0.2);
  display: flex;
  align-items: center;
  justify-content: center;
  color: rgba(0, 0, 0, 0.5);
  margin-right: 15px;
`;
const Add = styled.div`
  opacity: 0.5;
  font-weight: 300;
`;
const UserWrap = styled.div`
  padding: 5px 0;
  display: flex;
  align-items: center;
`;
const UserImg = styled.div`
  width: 30px;
  height: 30px;
  border-radius: 50%;
  background-color: gray;
  margin-right: 15px;
`;
const UserName = styled.div`
  font-size: 20px;
  font-weight: 400;
  opacity: 0.7;
`;

export const MessengerOption = () => {
  return (
    <Wrap>
      <Form>
        <Input placeholder="대화 내용 검색"></Input>
        <Button>
          <span>
            <FontAwesomeIcon icon={faSearch} />
          </span>
        </Button>
      </Form>
      <MenuTitle>
        <span>대화상대</span>
        <FontAwesomeIcon icon={faChevronUp} />
      </MenuTitle>
      <RoomEnterUserWrap>
        <AddUserWrap>
          <Icon>
            <FontAwesomeIcon icon={faPlus} />
          </Icon>
          <Add>대화 상대 초대</Add>
        </AddUserWrap>
        <UserWrap>
          <UserImg />
          <UserName>김충전 소장</UserName>
        </UserWrap>
      </RoomEnterUserWrap>
      <MenuTitle>
        <span>사진파일</span>
        <FontAwesomeIcon icon={faChevronUp} />
      </MenuTitle>
      <MenuTitle>
        <span>첨부파일</span>
        <FontAwesomeIcon icon={faChevronUp} />
      </MenuTitle>
    </Wrap>
  );
};
