import {
  faCalendarDays,
  faComment,
  faGear,
  faPaperclip,
  faSitemap,
  faVolumeHigh,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import { faBell } from "@fortawesome/free-solid-svg-icons";

const Section = styled.div`
  width: 5%;
  min-width: 80px;
  background-color: #234ca6;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding: 20px 0;
`;
const TopArea = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
const BottomArea = styled.div``;
const MenuWrap = styled.div`
  margin-bottom: 30px;
  color: rgba(255, 255, 255, 0.8);
  font-size: 26px;
  &:last-child {
    margin-bottom: 0;
  }
`;
const Img = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  background-color: #333;
`;

export const MessengerStatus = () => {
  return (
    <Section>
      <TopArea>
        <MenuWrap>
          <Img />
        </MenuWrap>
        <MenuWrap>
          <FontAwesomeIcon icon={faCalendarDays} />
        </MenuWrap>
        <MenuWrap>
          <FontAwesomeIcon icon={faComment} />
        </MenuWrap>
        <MenuWrap>
          <FontAwesomeIcon icon={faPaperclip} />
        </MenuWrap>
      </TopArea>
      <BottomArea>
        <MenuWrap>
          <FontAwesomeIcon icon={faVolumeHigh} />
        </MenuWrap>
        <MenuWrap>
          <FontAwesomeIcon icon={faSitemap} />
        </MenuWrap>
        <MenuWrap>
          <FontAwesomeIcon icon={faBell} />
        </MenuWrap>
        <MenuWrap>
          <FontAwesomeIcon icon={faGear} />
        </MenuWrap>
      </BottomArea>
    </Section>
  );
};
