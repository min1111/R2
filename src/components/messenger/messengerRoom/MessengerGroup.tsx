import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import { faBell, faPlus } from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";

const Wrap = styled.div`
  width: 25%;
  height: 100%;
  background-color: white;
  padding: 20px 30px 0 30px;
  overflow-y: scroll;
  position: relative;
  border-right: 1px solid rgba(0, 0, 0, 0.2);
`;
const Input = styled.input`
  all: unset;
  width: 100%;
  height: 50px;
  background-color: #eeeeee;
  padding-left: 15px;
  margin-bottom: 20px;
  &::placeholder {
    opacity: 0.5;
  }
`;
const ListWrap = styled.div`
  width: 100%;
`;

const MenuTitle = styled.div`
  color: black;
  opacity: 0.5;
  font-size: 16px;
`;
const ListConWrap = styled.div`
  padding: 25px 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
const LeftCon = styled.div`
  display: flex;
`;
const Img = styled.div`
  width: 60px;
  height: 60px;
  border-radius: 50%;
  background-color: #333;
  margin-right: 20px;
`;
const NameWrap = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
`;
const Name = styled.div`
  color: #333;
  font-size: 24px;
  font-weight: 600;
`;
const My = styled.div`
  display: inline-block;
  width: 40px;
  height: 25px;
  border-radius: 12.5px;
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 1px;
  border: 1px solid #234ca6;
  color: #234ca6;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  margin-left: 10px;
`;
const Bell = styled.div`
  display: inline-block;
  font-size: 20px;
  font-weight: 500;
  color: black;
  opacity: 0.4;
  margin-left: 10px;
`;
const Status = styled.div`
  color: #333;
  font-size: 16px;
  font-weight: 300;
`;
const Time = styled.div`
  color: #333;
  font-size: 16px;
  font-weight: 100;
  opacity: 0.5;
`;
const FixedMemberWrap = styled.div``;
const ChatListWrap = styled.div`
  cursor: pointer;
`;
const AddButton = styled.div`
  width: 80px;
  height: 80px;
  border-radius: 50%;
  background-color: #234ca6;
  position: absolute;
  bottom: 20px;
  right: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 40px;
  color: rgba(255, 255, 255, 0.8);
`;

interface PropsType {
  roomlist: any[];
  user: any;
}

export const MessengerGroup = ({ roomlist, user }: PropsType) => {
  const enterRoom = (roomid: any) => {};
  return (
    <Wrap>
      <Input placeholder="채팅방 , 참여자 검색"></Input>
      <ListWrap>
        <MenuTitle>상단 고정 대화방</MenuTitle>
        <FixedMemberWrap>
          <ListConWrap>
            <LeftCon>
              <Img />
              <NameWrap>
                <Name>
                  <span>{user?.username}</span>
                  <My>MY</My>
                </Name>
                <Status>나와의 대화내용</Status>
              </NameWrap>
            </LeftCon>
            <Time>3시간전</Time>
          </ListConWrap>
        </FixedMemberWrap>
        <MenuTitle>대화목록</MenuTitle>
        {/* ============================== 여기가 대화목록 */}
        {roomlist.map((list: any) => (
          <ChatListWrap
            onClick={() => {
              enterRoom(`${list.id}`);
            }}
            key={list.id}
          >
            <ListConWrap>
              <LeftCon>
                <Img style={{ backgroundColor: "gray" }} />
                <NameWrap>
                  <Name>
                    <span>{list.opponentName}</span>
                    <Bell>
                      <FontAwesomeIcon icon={faBell} />
                    </Bell>
                  </Name>
                  <Status>1:1 대화내용</Status>
                </NameWrap>
              </LeftCon>
              <Time>3분 전</Time>
            </ListConWrap>
          </ChatListWrap>
        ))}
      </ListWrap>
      <AddButton>
        <FontAwesomeIcon icon={faPlus} />
      </AddButton>
    </Wrap>
  );
};
