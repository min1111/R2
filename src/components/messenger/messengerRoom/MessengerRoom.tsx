import { useSelector } from "react-redux";
import styled from "styled-components";
import { MessengerChat } from "./MessengerChat";
import { MessengerGroup } from "./MessengerGroup";
import { MessengerOption } from "./MessengerOption";
import { MessengerStatus } from "./MessengerStatus";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { stompClient } from "../../sock/SockBell";
import { Loading } from "../../Loading";
import SockJS from "sockjs-client";
import { Stomp } from "@stomp/stompjs";

const Wrap = styled.div`
  width: 100%;
  height: 100vh;
  min-height: 950px;
  display: flex;
  padding-top: 200px;
`;

export const MessengerRoom = () => {
  const [loading, setLoading] = useState(false);

  const user: any = useSelector(
    (state: any) => state?.user?.loginSuccess?.data?.result
  );

  const roomlist: any = useSelector(
    (state: any) => state?.sock?.chatroomlist?.result
  );
  const test: any = useSelector((state: any) => state?.sock?.sockstatus);
  console.log(test);
  const params = useParams();

  useEffect(() => {
    const enterRoom = () => {
      if (stompClient) {
        stompClient.send(`/app/getRooms/${params?.id}`, {}, null);
        stompClient.send(`/app/getChatList/${user?.email}`, {}, null);
        setLoading(true);
      }
    };

    console.log(user);

    enterRoom();
  }, [test]);

  return (
    <>
      {loading ? (
        <Wrap>
          <MessengerStatus />
          {roomlist ? (
            <MessengerGroup roomlist={roomlist} user={user} />
          ) : (
            <Loading />
          )}

          <MessengerChat loading={loading} user={user} />
          <MessengerOption />
        </Wrap>
      ) : (
        <div style={{ width: "100%", height: "100vh" }}>
          <Loading />
        </div>
      )}
    </>
  );
};
