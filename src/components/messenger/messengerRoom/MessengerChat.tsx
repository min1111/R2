import { faFaceLaugh, faPaperPlane } from "@fortawesome/free-regular-svg-icons";
import {
  faBell,
  faShareFromSquare,
  faLink,
  faExpand,
  faThumbtack,
  faLocationArrow,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import { useState, useEffect, useRef } from "react";
import { Loading } from "../../Loading";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { stompClient } from "../../sock/SockBell";

const Wrap = styled.section`
  width: 45%;
  background-color: white;
  border-right: 1px solid rgba(0, 0, 0, 0.2);
`;

const RommStatuWrap = styled.div`
  width: 100%;
  height: 10%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: rgba(110, 126, 156, 0.4);
  padding: 0 20px;
`;

const EnterUserWrap = styled.div``;
const EnterUser = styled.div`
  display: flex;
  align-items: center;
`;
const Img = styled.div`
  width: 60px;
  height: 60px;
  border-radius: 50%;
  background-color: gray;
  margin-right: 20px;
`;
const Name = styled.div`
  color: #333;
  font-size: 24px;
  font-weight: 600;
`;

const IconWrap = styled.div`
  display: flex;
  align-items: center;
`;
const Icon = styled.div`
  font-size: 26px;
  margin-left: 22px;
  opacity: 0.2;
`;
const ChatArea = styled.div`
  width: 100%;
  height: 75%;
  overflow-y: scroll;
  padding: 0 20px;
`;

const UserTextWrap = styled.div`
  display: flex;
  width: 100%;
  margin-top: 7.5px;
  margin-bottom: 7.5px;
`;

const UserText = styled.div`
  font-size: 20px;
  max-width: 70%;
`;
const UserName = styled.div`
  opacity: 0.4;
  font-weight: 500;
  margin-bottom: 10px;
`;
const Text = styled.div`
  font-size: 20px;
  background-color: rgba(110, 126, 156, 0.4);
  color: #1d1d1d;
  padding: 10px 20px;
  border-radius: 20px;
`;

const MyTextWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: right;
  padding: 5px 0;
  span {
    max-width: 70%;
    font-size: 20px;
    background-color: #234ca6;
    color: white;
    padding: 10px 20px;
    border-radius: 20px;
  }
`;
const InputArea = styled.form`
  width: 100%;
  height: 15%;
`;
const Input = styled.textarea`
  all: unset;
  display: block;
  width: 100%;
  height: 70%;
  background-color: white;
  border-top: 1px solid rgba(0, 0, 0, 0.2);

  box-sizing: border-box;
  padding: 10px;
  &::placeholder {
    opacity: 0.4;
  }
`;
const ButtonWrap = styled.div`
  width: 100%;
  height: 30%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 20px;
`;
const AddFileWrap = styled.div`
  display: flex;
`;
const AddFile = styled.div`
  font-size: 22px;
  margin-right: 20px;
  opacity: 0.2;
`;
const Button = styled.button`
  all: unset;
  font-size: 28px;
  color: #234ca6;
  transform: rotateZ(45deg);
`;

interface PropsType {
  loading: boolean;
  user: any;
}

export const MessengerChat = ({ loading, user }: PropsType) => {
  const [text, setText] = useState<string>("");
  const [chatlist, setChatList] = useState<
    { id: number; userEmail: string; message: string; room: any }[]
  >([]);
  const chat: any = useSelector((state: any) => state?.sock?.enter);

  const pushchat: any = useSelector(
    (state: any) => state?.sock?.pushmessage?.result
  );
  const params = useParams();

  useEffect(() => {
    const chatlistDb = () => {
      setChatList(chat);
    };
    chatlistDb();
  }, [chat]);

  useEffect(() => {
    setChatList([...chatlist, pushchat]);
  }, [pushchat]);

  const submit = (e: any) => {
    e.preventDefault();

    if (text) {
      const formdata = {
        userEmail: user?.email,
        message: text,
      };
      stompClient.send(
        `/app/postChat/${params.id}`,
        {},
        JSON.stringify(formdata)
      );
    }
    setText("");
  };
  console.log(chatlist);
  const scrollRef: any = useRef();

  useEffect(() => {
    scrollRef.current.scrollTop = scrollRef.current.scrollHeight;
  });

  return (
    <Wrap>
      <RommStatuWrap>
        <EnterUserWrap>
          <EnterUser>
            <Img />
            <Name>김충전</Name>
          </EnterUser>
        </EnterUserWrap>
        <IconWrap>
          <Icon>
            <FontAwesomeIcon icon={faThumbtack} />
          </Icon>
          <Icon>
            <FontAwesomeIcon icon={faBell} />
          </Icon>
          <Icon>
            <FontAwesomeIcon icon={faShareFromSquare} />
          </Icon>
        </IconWrap>
      </RommStatuWrap>
      <ChatArea ref={scrollRef}>
        {chatlist ? (
          <>
            {chatlist?.map(
              (chat: {
                id: number;
                userEmail: string;
                message: string;
                room: any;
              }) => (
                <div key={chat?.id}>
                  {chat?.userEmail === user?.email ? (
                    <MyTextWrap key={chat?.id}>
                      <span>{chat?.message}</span>
                    </MyTextWrap>
                  ) : (
                    <UserTextWrap key={chat?.id}>
                      <Img style={{ marginRight: "10px" }}></Img>
                      <UserText>
                        <UserName>김충전</UserName>
                        <Text>{chat?.message}</Text>
                      </UserText>
                    </UserTextWrap>
                  )}
                </div>
              )
            )}
          </>
        ) : (
          <Loading />
        )}
      </ChatArea>
      <InputArea onSubmit={submit}>
        <Input
          placeholder="메세지를 입력하세요"
          value={text}
          onChange={(e: any) => {
            setText(e.target.value);
          }}
        ></Input>
        <ButtonWrap>
          <AddFileWrap>
            <AddFile>
              <FontAwesomeIcon icon={faFaceLaugh} />
            </AddFile>
            <AddFile>
              <FontAwesomeIcon icon={faLink} />
            </AddFile>
            <AddFile>
              <FontAwesomeIcon icon={faExpand} />
            </AddFile>
          </AddFileWrap>
          <Button>
            <FontAwesomeIcon icon={faLocationArrow} />
          </Button>
        </ButtonWrap>
      </InputArea>
    </Wrap>
  );
};
