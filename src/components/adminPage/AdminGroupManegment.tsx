import styled from "styled-components";
import { useState } from "react";
import MenuItem from "@mui/material/MenuItem";
import Select, { SelectChangeEvent } from "@mui/material/Select";

const Container = styled.div`
  width: 60%;
  height: 400px;
  background-color: white;
  border-radius: 25px;
  overflow: hidden;
  position: relative;
  margin-bottom: 100px;
`;

const Title = styled.div`
  width: 100%;
  height: 80px;
  background-color: #002d7d;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 30px;
  color: white;
  font-size: 28px;
  div {
    font-size: 24px;
    background-color: white;
    border-radius: 15px;
    width: 200px;
    height: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #002d7d;
    font-weight: 500;
  }
`;

const ConWrap = styled.div`
  width: 100%;
  padding: 30px;
  height: 100%;
  overflow-y: scroll;
  &::-webkit-scrollbar {
    width: 10px;
  }
  &::-webkit-scrollbar-thumb {
    background-color: #002d7d;
    border-radius: 25px;
  }
`;

const TableWrap = styled.table`
  width: 100%;
`;
const Head = styled.thead`
  width: 100%;
  height: 30px;
  background-color: #002d7d;
  color: white;

  th {
    border-right: 1px solid white;
    &:first-child {
      border-left: 1px solid #002d7d;
    }
    &:last-child {
      border-right: 1px solid #002d7d;
    }
  }
`;
const Body = styled.tbody`
  th {
    border-right: 1px solid #888;
    border-bottom: 1px solid #888;
    height: 60px;
    &:first-child {
      border-left: 1px solid #888;
    }
  }
`;
const Row = styled.tr`
  width: 100%;
  text-align: center;
  height: 30px;
`;
const Cell = styled.th`
  vertical-align: middle;
  &:nth-child(1) {
    width: 7.5%;
  }
  &:nth-child(2) {
    width: 20%;
  }
  &:nth-child(3) {
    width: 30%;
  }
  &:nth-child(4) {
    width: 15%;
  }
  &:nth-child(5) {
    width: 10%;
  }
  &:nth-child(6) {
    width: 17.5%;
  }
`;

export const AdminGroupManegment = () => {
  const [age, setAge] = useState("");

  const handleChange = (event: SelectChangeEvent) => {
    setAge(event.target.value);
  };
  return (
    <Container>
      <Title>
        <span>조직 관리</span>
        <div>조직 추가</div>
      </Title>
      <ConWrap>
        <TableWrap>
          <Head>
            <Row>
              <Cell>ID</Cell>
              <Cell>조직명</Cell>
              <Cell>주소</Cell>
              <Cell>전화번호</Cell>
              <Cell>상태</Cell>
              <Cell>최종 업데이트일</Cell>
            </Row>
          </Head>
          <Body>
            <Row>
              <Cell>1</Cell>
              <Cell>Pacom</Cell>
              <Cell>수원시 장안구 정자2동</Cell>
              <Cell>031-123-1234</Cell>
              <Cell>
                <Select
                  style={{
                    width: "100%",
                    padding: "0",
                    height: "100%",
                    fontWeight: "900",
                  }}
                  value={age}
                  label="use"
                  onChange={handleChange}
                >
                  <MenuItem value={"사용중"}>사용중</MenuItem>
                  <MenuItem value={"사용중지"}>사용중지</MenuItem>
                </Select>
              </Cell>
              <Cell>2023-01-01 22:18:39</Cell>
            </Row>
            <Row>
              <Cell>2</Cell>
              <Cell>Pacom(R&D)</Cell>
              <Cell>수원시 장안구 정자2동</Cell>
              <Cell>031-321-4321</Cell>
              <Cell>
                <Select
                  style={{
                    width: "100%",
                    padding: "0",
                    height: "100%",
                    fontWeight: "900",
                  }}
                  value={age}
                  label="use"
                  onChange={handleChange}
                >
                  <MenuItem value={"사용중"}>사용중</MenuItem>
                  <MenuItem value={"사용중지"}>사용 중지</MenuItem>
                </Select>
              </Cell>
              <Cell>2023-09-11 22:18:39</Cell>
            </Row>
          </Body>
        </TableWrap>
      </ConWrap>
    </Container>
  );
};
