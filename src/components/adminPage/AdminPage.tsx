import styled from "styled-components";
import { AdminGroupManegment } from "./AdminGroupManegment";
import { AdminPageChat } from "./AdminPageChat";
import { AdminUpload } from "./AdminUpload";
import { AdminUserManegement } from "./AdminUserManegement";

const Section = styled.section`
  padding: 200px 120px 0 120px;
  width: 100%;
`;

export const AdminPage = () => {
  return (
    <Section>
      <AdminPageChat />
      <AdminGroupManegment />
      <AdminUserManegement />
      <AdminUpload />
    </Section>
  );
};
