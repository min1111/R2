import styled from "styled-components";
import { useEffect, useState } from "react";
import { authApi } from "../../api";
import { setToken } from "../login/LoginInput";
import { Loading } from "../Loading";
import { useForm } from "react-hook-form";
import { useParams, useNavigate } from "react-router-dom";

const Section = styled.section`
  width: 100%;
  min-height: 100vh;
  padding: 200px 120px;
`;
const Container = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 25px;
  overflow: hidden;
`;
const Title = styled.div`
  width: 100%;
  height: 100px;
  text-align: center;
  background-color: #002d7d;
  border-top-left-radius: 25px;
  border-top-right-radius: 25px;
  font-size: 42px;
  font-weight: 600;
  color: white;
  line-height: 84px;
`;
const ConWrap = styled.div`
  width: 100%;
  height: 800px;
  background-color: white;
`;
const Form = styled.form`
  width: 100%;
  height: 100%;
  display: flex;

  justify-content: center;
`;
const LeftConWrap = styled.div`
  width: 60%;
  padding: 50px;
`;
const InputWrap = styled.div`
  width: 100%;
  margin-bottom: 30px;
  &:nth-child(2) {
    input {
      background-color: #002d7d;
      color: white;
    }
  }
`;
const Label = styled.label`
  display: block;
  font-size: 18px;
  font-weight: 300;
  color: rgba(0, 0, 0, 0.5);
  margin-bottom: 10px;
`;
const Input = styled.input`
  all: unset;
  width: 100%;
  height: 40px;
  border: 1px solid gray;
  border-radius: 10px;
  padding-left: 10px;
`;
const Slect = styled.select`
  width: 100%;
  height: 40px;
  border-radius: 10px;
  padding-left: 5px;
`;
const Option = styled.option``;
const Button = styled.button`
  all: unset;
  width: 100%;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #002d7d;
  color: white;
  border-radius: 10px;
  margin-top: 100px;
`;
const RightConWrap = styled.div`
  width: 40%;
  height: 100%;
  background-color: gray;
`;

interface dataType {
  email: string;
  id: number;
  password?: any;
  phone: string;
  profil: string;
  role: string;
  roleList?: any;
  token?: any;
  username: string;
}

export const AdminUserDetail = () => {
  const [userInfo, setUserInfo] = useState<dataType | null>(null);
  const { register, handleSubmit, getValues } = useForm({
    mode: "onChange",
  });

  const params = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    const list = async () => {
      const {
        data: { result },
      } = await authApi()
        .get(`/api/v1/user/getUser/${params.id}`)
        .then((res: any) => {
          setToken(res.headers.authorization);
          authApi();
          return res;
        });
      setUserInfo(result);
    };
    list();
  }, []);
  console.log(userInfo);

  const submit = () => {
    const { username, email, phone, password, passwordCheck, role } =
      getValues();
    console.log(role);
    if (password || passwordCheck) {
      // ====================================== 패스워드가 있을 경우
      if (password !== passwordCheck) {
        // ====================================== 패스워드가 일치하지 않을 경우
        alert("패스워드가 일치하지 않습니다.");
      } else {
        // ====================================== 패스워드가 일치할 경우
        const data = {
          username: username,
          email: email,
          phone: phone,
          role: role,
          password: password,
          passwordCheck: passwordCheck,
        };

        authApi()
          .put(`/api/v1/user/userUpdate/${params.id}`, null, { params: data })
          .then((res: any) => {
            setToken(res.headers.authorization);
            authApi();
            return res;
          });
        navigate("/admin");
      }
    } else {
      // ====================================== 패스워드가 없을경우
      const data = {
        username: username,
        email: email,
        phone: phone,
        role: role,
      };
      authApi()
        .put(`/api/v1/user/userUpdate/${params.id}`, null, { params: data })
        .then((res: any) => {
          setToken(res.headers.authorization);
          authApi();
          return res;
        });
      navigate("/admin");
    }
  };

  return (
    <Section>
      <Container>
        <Title>관리자 권한 설정</Title>
        <ConWrap>
          {userInfo !== null ? (
            <Form onSubmit={handleSubmit(submit)}>
              <LeftConWrap>
                <InputWrap>
                  <Label>이름</Label>
                  <Input
                    type="text"
                    defaultValue={userInfo?.username}
                    {...register("username")}
                  ></Input>
                </InputWrap>
                <InputWrap>
                  <Label>이메일</Label>
                  <Input
                    type="text"
                    value={userInfo?.email}
                    {...register("email")}
                  ></Input>
                </InputWrap>
                <InputWrap>
                  <Label>전화번호</Label>
                  <Input
                    type="text"
                    defaultValue={userInfo?.phone}
                    {...register("phone")}
                  ></Input>
                </InputWrap>
                <InputWrap>
                  <Label>비밀번호</Label>
                  <Input type="password" {...register("password")}></Input>
                </InputWrap>
                <InputWrap>
                  <Label>비밀번호 확인</Label>
                  <Input type="password" {...register("passwordCheck")}></Input>
                </InputWrap>
                <InputWrap>
                  <Label>권한 설정</Label>
                  <Slect defaultValue={userInfo?.role} {...register("role")}>
                    <Option value={"ROLE_ADMIN"}>슈퍼 관리자</Option>
                    <Option value={"ROLE_MANAGER"}>관리자</Option>
                    <Option value={"ROLE_USER"}>일반 회원</Option>
                  </Slect>
                </InputWrap>
                <Button>수정하기</Button>
              </LeftConWrap>
              <RightConWrap></RightConWrap>
            </Form>
          ) : (
            <Loading />
          )}
        </ConWrap>
      </Container>
    </Section>
  );
};
