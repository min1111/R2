import styled from "styled-components";
import { useEffect, useState } from "react";
import { useWatch, useForm } from "react-hook-form";
import { fileUploadApi } from "../../api";
import { setToken } from "../login/LoginInput";

const Container = styled.div`
  width: 60%;
  height: 350px;
  background-color: white;
  border-radius: 25px;
  overflow: hidden;
  position: relative;
`;

const Title = styled.div`
  width: 100%;
  height: 100px;
  background-color: #002d7d;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 30px;
  color: white;
  font-size: 28px;
`;

const ConWrap = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const InputWrap = styled.form`
  width: 50%;
  padding: 30px;
`;
const Input = styled.input`
  display: none;
`;

const Label = styled.label`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 200px;
  background-color: #002d7d;
  border-radius: 25px;
  font-size: 36px;
  text-align: center;
  line-height: 50px;
  color: white;
`;
const SubmitWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;
const Button = styled.button`
  all: unset;
  display: flex;
  margin-top: 40px;
  font-size: 22px;
  width: 50%;
  height: 50px;
  background-color: rgb(13, 61, 144);
  align-items: center;
  justify-content: center;
  border-radius: 15px;
`;

export const AdminUpload = () => {
  const { watch, register, getValues } = useForm({ mode: "onChange" });

  const Preview = watch("file");

  const [fileName, setFileName] = useState([]);

  useEffect(() => {
    if (Preview && Preview.length > 0) {
      // ====================================================
      let nameList: any = [];
      for (let i: number = 0; i < Preview.length; i++) {
        nameList.push(Preview[i].name);
      }
      setFileName(nameList);

      //===========================================파일이름 저장
    }
  }, [Preview]);

  const submit = async (e: any) => {
    e.preventDefault();
    const { file } = getValues();

    console.log(getValues());

    let formData = new FormData();

    formData.append("file", file[0]);

    const Db: any = await fileUploadApi
      .post("/excel/read", formData, {
        headers: {
          Authorization: `${
            localStorage.getItem("authorization")
              ? localStorage.getItem("authorization")
              : "Bearer"
          }`,
        },
      })
      .then((res: any) => {
        setToken(res.headers.authorization);
        return res;
      })
      .catch((err) => console.log(err));

    console.log(Db);

    if (Db.data.code === 1) {
      alert("업로드 성공");
    } else {
    }
  };
  return (
    <Container>
      <Title>관리자 파일 업로드</Title>
      <ConWrap>
        <InputWrap onSubmit={submit}>
          <Input
            type={"file"}
            accept=".xls,.xlsx"
            id="file"
            {...register("file")}
          ></Input>
          <Label htmlFor="file">
            {fileName.length > 0 ? (
              <>
                {fileName.map((name) => (
                  <SubmitWrap key={name}>
                    <p style={{ fontSize: "32px" }}>파일명 : {name}</p>
                    <Button>제출하기</Button>
                  </SubmitWrap>
                ))}
              </>
            ) : (
              <p>
                음료 데이터 <br />
                수기 파일 업로드
              </p>
            )}
          </Label>
        </InputWrap>
        <InputWrap onSubmit={submit}>
          <Input
            type={"file"}
            accept=".xls,.xlsx"
            id="file"
            {...register("file")}
          ></Input>
          <Label htmlFor="file">
            {fileName.length > 0 ? (
              <>
                {fileName.map((name) => (
                  <SubmitWrap key={name}>
                    <p style={{ fontSize: "32px" }}>파일명 : {name}</p>
                    <Button>제출하기</Button>
                  </SubmitWrap>
                ))}
              </>
            ) : (
              <p>
                수소 충전소 <br />
                수기 파일 업로드
              </p>
            )}
          </Label>
        </InputWrap>
      </ConWrap>
    </Container>
  );
};
