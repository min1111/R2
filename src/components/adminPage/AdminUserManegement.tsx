import styled from "styled-components";
import { useEffect, useState } from "react";
import { useWatch, useForm } from "react-hook-form";
import { fileUploadApi, authApi } from "../../api";
import { setToken } from "../login/LoginInput";
import { Link } from "react-router-dom";
import { Loading } from "../Loading";

const Container = styled.div`
  width: 60%;
  height: 350px;
  background-color: white;
  border-radius: 25px;
  overflow: hidden;
  position: relative;
  margin-bottom: 100px;
`;

const Title = styled.div`
  width: 100%;
  height: 100px;
  background-color: #002d7d;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 30px;
  color: white;
  font-size: 28px;
  div {
    font-size: 24px;
    background-color: white;
    border-radius: 15px;
    width: 200px;
    height: 50px;
    display: flex;
    align-items: center;
    justify-content: center;
    color: #002d7d;
    font-weight: 500;
  }
`;

const ConWrap = styled.div`
  width: 100%;
  height: 250px;
  padding: 0 30px;
  overflow-y: scroll;
`;

const TableWrap = styled.table`
  width: 100%;
  margin: 30px auto;
`;
const Head = styled.thead`
  width: 100%;
  height: 30px;
  background-color: #002d7d;
  color: white;

  th {
    border-right: 1px solid white;
    &:first-child {
      border-left: 1px solid #002d7d;
    }
    &:last-child {
      border-right: 1px solid #002d7d;
    }
  }
`;
const Body = styled.tbody`
  th {
    border-right: 1px solid #888;
    border-bottom: 1px solid #888;
    height: 60px;
    &:first-child {
      border-left: 1px solid #888;
    }
  }
`;
const Row = styled.tr`
  width: 100%;
  text-align: center;
  height: 30px;
`;
const Cell = styled.th`
  vertical-align: middle;
  &:nth-child(1) {
    width: 7.5%;
  }
  &:nth-child(2) {
    width: 27.5%;
  }
  &:nth-child(3) {
    width: 35%;
  }
  &:nth-child(4) {
    width: 15%;
  }
  &:nth-child(5) {
    width: 15%;
  }
`;

export const AdminUserManegement = () => {
  const [adminList, setAdminList] = useState<any[]>([]);

  useEffect(() => {
    const list = async () => {
      const {
        data: { results },
      } = await authApi()
        .get("/api/v1/user/userList")
        .then((res: any) => {
          setToken(res.headers.authorization);
          authApi();
          return res;
        });
      setAdminList(results);
    };
    list();
  }, []);

  return (
    <Container>
      <Title>
        <span>관리자 권한 관리</span>
        <div>관리자 추가</div>
      </Title>
      <ConWrap>
        <TableWrap>
          <Head>
            <Row>
              <Cell>ID</Cell>
              <Cell>관리자명</Cell>
              <Cell>이메일</Cell>
              <Cell>전화번호</Cell>
              <Cell>권한</Cell>
            </Row>
          </Head>
          <Body>
            {adminList.length > 0 ? (
              adminList.map((data) => (
                <Row key={data.id}>
                  <Cell>{data.id}</Cell>
                  <Cell>
                    <Link
                      to={`/admin/user/detail/${data.id}`}
                      style={{
                        textDecoration: "underline",
                        fontWeight: "600",
                        fontSize: "18px",
                      }}
                    >
                      {data.username}
                    </Link>
                  </Cell>
                  <Cell>{data.email}</Cell>
                  <Cell>{data.phone}</Cell>
                  <Cell>
                    {data.role === "ROLE_USER"
                      ? "일반유저"
                      : `${
                          data.role === "ROLE_MANAGER"
                            ? "매니저"
                            : "슈퍼 관리자"
                        }`}
                  </Cell>
                </Row>
              ))
            ) : (
              <Loading />
            )}
          </Body>
        </TableWrap>
      </ConWrap>
    </Container>
  );
};
