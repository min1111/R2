import styled from "styled-components";
import { useState, useEffect, useRef } from "react";
import { element } from "prop-types";

import { Stomp } from "@stomp/stompjs";
import SockJS from "sockjs-client";
import { useDispatch, useSelector } from "react-redux";
// import { stompClient } from "../sock/SockBell";
import { loginApi, authApi } from "../../api";
import { stompClient } from "../sock/SockBell";

const Container = styled.div`
  position: fixed;
  top: 200px;
  right: 120px;
  width: 30%;
  height: 70vh;
  background-color: white;
  border-radius: 25px;
  overflow: hidden;

  margin-bottom: 150px;
`;

const Title = styled.div`
  width: 100%;
  height: 15%;
  background-color: #002d7d;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  font-size: 40px;
`;

const ChatWrap = styled.div`
  width: 100%;
  height: 60%;
  background-color: white;
  overflow-y: scroll;
  padding: 30px;
  position: relative;
  top: 0;
  left: 0;
  &::-webkit-scrollbar {
    width: 20px;
  }
  &::-webkit-scrollbar-thumb {
    background-color: #002d7d;
    border-radius: 25px;
  }
`;

const Chat = styled.div`
  background-color: #002d7d;
  padding: 15px;
  font-size: 28px;
  margin-bottom: 30px;
  display: table;
  border-radius: 15px;
  color: white;
`;
const InputWrap = styled.form`
  width: 100%;
  height: 25%;
  position: relative;
  textarea {
    all: unset;
    box-sizing: border-box;
    width: 95%;
    height: 95%;
    border-top-right-radius: 30px;
    border-bottom-right-radius: 30px;

    border: 3px solid #002d7d;
    border-left: none;
    padding: 20px 30px;
    font-size: 22px;
  }
  button {
    width: 50px;
    height: 50px;
    border-radius: 50%;
    background-color: #002d7d;
    border: none;
    color: white;
    position: absolute;
    bottom: 15%;
    right: 10%;
  }
`;

export const AdminPageChat = () => {
  const [text, setText] = useState("");
  const [chatDb, setChatDb] = useState([{ message: "공지사항" }]);

  const submit = async (e: any) => {
    e.preventDefault();

    if (text) {
      const chatMessage: { senderName: string; message: string } = {
        senderName: "123@123.com",
        message: text,
      };
      stompClient.send("/app/message", {}, JSON.stringify(chatMessage));
      setChatDb([...chatDb, { message: text }]);
      setText("");
    }
  };

  const scrollRef: any = useRef();

  useEffect(() => {
    scrollRef.current.scrollTop = scrollRef.current.scrollHeight;
  });

  useEffect(() => {
    const countMessage = {
      receiverName: "123@123.com",
    };
    const data = async () => {
      const {
        data: { result: Db },
      } = await loginApi.post("/adminmessage", countMessage, {
        headers: {
          "content-type": "application/x-www-form-urlencoded;charset=utf-8",
        },
      });

      setChatDb(Db);
    };
    data();
  }, []);

  return (
    <Container>
      <Title>관리자 공지사항</Title>

      <ChatWrap ref={scrollRef}>
        {chatDb.map((chat) => (
          <Chat>{chat.message}</Chat>
        ))}
      </ChatWrap>
      <InputWrap onSubmit={submit}>
        <textarea
          onChange={(e) => {
            setText(e.target.value);
          }}
          value={text}
          placeholder="본문내용을 작성해 주세요"
          cols={100}
          rows={10}
        ></textarea>
        <button>전송</button>
      </InputWrap>
    </Container>
  );
};
