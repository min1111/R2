import { useSelector } from "react-redux";
import styled from "styled-components";

const Wrap = styled.footer`
  width: 100%;
  height: 120px;
  background-color: #001c4d;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  font-size: 20px;
  margin-top: 100px;
`;

export const Footer = () => {
  const user: any = useSelector(
    (state: any) => state?.user?.loginSuccess?.data
  );
  return <>{user?.code === 1 ? <Wrap>FooterArea</Wrap> : ""}</>;
};
