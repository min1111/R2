import { faPhone } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import { useEffect, useState } from "react";
import { setToken } from "../../login/LoginInput";
import { authApi } from "../../../api";

const BoxWrap = styled.div`
  width: 300px;
  height: 200px;
`;
const Title = styled.div`
  /* margin-top: 0px; */
  font-size: 18px;
  font-weight: 600;
`;
const ConWrap = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: left;
  margin-top: 10px;
  margin-bottom: 15px;
`;
const Status = styled.div`
  min-width: 100px;
  height: 100px;
  border-radius: 50%;
  background-color: green;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  font-weight: 500;
  color: white;
`;
const TextWrap = styled.div`
  padding-left: 40px;
`;
const Price = styled.div`
  font-size: 16px;
  margin-bottom: 10px;
  span {
    font-size: 18px;
    font-weight: 600;
    margin-left: 10px;
  }
`;

const Adress = styled.div`
  font-size: 18px;
  font-weight: 400;
  margin-bottom: 5px;
`;

const Phone = styled.div`
  font-size: 10px;
  margin-bottom: 10px;

  span {
    font-size: 14px;
    margin-left: 10px;
  }
`;

interface propType {
  name: string;
  number: string;
  price: number;
  address: string;
  id: string;
  // data: any;
}

export const GoogleMapMarker = ({
  name,
  number,
  price,
  address,
  id,
}: propType) => {
  const [data, setData] = useState<any>({
    congestion: "",
    cumulativeSales: "",
    id: "",
  });

  useEffect(() => {
    const mapApi = async () => {
      const {
        data: { results: data },
      } = await authApi().get(`/api/v1/user/getHydrogen2/${id}`);

      setData(data[0]);
    };
    setInterval(() => {
      mapApi();
    }, 1000);
  }, []);
  console.log(data);
  return (
    <BoxWrap>
      <Title> {name}</Title>
      <ConWrap>
        <Status
          style={{
            backgroundColor: `${
              data.congestion === "1"
                ? `green`
                : `${data.congestion === "2" ? `orange` : `red`}`
            }`,
          }}
        >
          {data.congestion === "1"
            ? `원활`
            : `${data.congestion === "2" ? `보통` : `혼잡`}`}
        </Status>
        <TextWrap>
          <Price>
            대기차량 수 :
            <span>
              {data.congestion === "1"
                ? `${Math.floor(Math.random() * (3 - 0 + 1)) + 0}`
                : `${
                    data.congestion === "2"
                      ? `${Math.floor(Math.random() * (9 - 4 + 1)) + 4}`
                      : `${Math.floor(Math.random() * (15 - 10 + 1)) + 10}`
                  }`}
            </span>
          </Price>
          <Price>
            가격 : <span>{price} 원</span>
          </Price>
        </TextWrap>
      </ConWrap>
      <Adress>
        <span>{address}</span>
      </Adress>
      <Phone>
        <FontAwesomeIcon icon={faPhone} />
        <span>{number}</span>
      </Phone>
    </BoxWrap>
  );
};
