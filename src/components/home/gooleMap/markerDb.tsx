export interface makerType {
  id: number;
  name: string;
  position: { lat: any; lng: any };
}

export const markerDb: makerType[] = [
  { id: 1, name: "수원동부", position: { lat: 37.2864672, lng: 127.0784113 } },
  { id: 2, name: "극동", position: { lat: 37.2681854, lng: 127.0059549 } },
  { id: 3, name: "화성동탄", position: { lat: 37.1772825, lng: 127.0858506 } },
  { id: 4, name: "양재", position: { lat: 37.4686233, lng: 127.034109 } },
  { id: 5, name: "화성향남", position: { lat: 37.1390526, lng: 126.9245565 } },
  { id: 6, name: "화성시청", position: { lat: 37.1991652, lng: 126.829891 } },
  { id: 7, name: "국회", position: { lat: 37.5282508, lng: 126.9152709 } },
  { id: 8, name: "에버랜드", position: { lat: 37.293284, lng: 127.2219001 } },
  { id: 9, name: "평택", position: { lat: 37.0530293, lng: 127.080169 } },
  { id: 10, name: "상암", position: { lat: 37.5686099, lng: 126.8782914 } },
  { id: 11, name: "반월", position: { lat: 37.2985688, lng: 126.9261263 } },
  { id: 12, name: "하남", position: { lat: 37.5309709, lng: 127.2084192 } },
  { id: 13, name: "성남", position: { lat: 37.4217501, lng: 127.1556785 } },
  { id: 14, name: "경기안산", position: { lat: 37.2960805, lng: 126.7934182 } },
  { id: 15, name: "강동", position: { lat: 37.5450485, lng: 127.1697735 } },
  { id: 16, name: "인천", position: { lat: 37.3985745, lng: 126.7115322 } },
  { id: 17, name: "수원", position: { lat: 37.2711821, lng: 126.9950542 } },
  {
    id: 18,
    name: "안성휴게소",
    position: { lat: 37.076956, lng: 127.1316327 },
  },
  { id: 19, name: "과천", position: { lat: 37.4557434, lng: 127.009914 } },
  { id: 20, name: "태양", position: { lat: 37.49858, lng: 126.661726 } },
  { id: 21, name: "팽성", position: { lat: 36.9546923, lng: 127.0605808 } },
  { id: 22, name: "평화", position: { lat: 36.9456246, lng: 126.9138097 } },
  { id: 23, name: "마곡", position: { lat: 37.5729542, lng: 126.8291683 } },
  { id: 24, name: "당진", position: { lat: 36.9745164, lng: 126.7065682 } },
  { id: 25, name: "은실", position: { lat: 37.003355, lng: 127.059422 } },
  { id: 26, name: "아산인주", position: { lat: 36.8488896, lng: 126.8565434 } },
  {
    id: 27,
    name: "시험용 대구 좌표",
    position: { lat: 35.8714354, lng: 128.6014451 },
  },
];
