import React from "react";
import {
  GoogleMap,
  useJsApiLoader,
  Marker,
  InfoWindow,
} from "@react-google-maps/api";
import styled from "styled-components";
import { markerDb } from "./markerDb";
import { useState, useEffect } from "react";
import { GraphTitle } from "../graphDashboard/GraphTitle";

import { GoogleMapMarker } from "./GoogleMapMarker";
import { authApi } from "../../../api";
import { setToken } from "../../login/LoginInput";
const TitleWrap = styled.div`
  width: 200px;
  margin-top: 50px;
  overflow: hidden;
  background-color: white;
  padding: 20px 0 10px 20px;
  border-top-right-radius: 15px;
  border-top-left-radius: 15px;
`;
const Wrap = styled.div`
  border-bottom-right-radius: 15px;
  border-top-right-radius: 15px;
  border-bottom-left-radius: 15px;
  overflow: hidden;
  background-color: white;
`;

const containerStyle = {
  width: "100%",
  height: "600px",
};

const center = {
  lat: 36.64114212395556,
  lng: 127.84432184246342,
};

export const GooleMap = () => {
  const [mapDb, setMapDb] = useState<any[]>([]);
  const [data, setData] = useState<any>({
    congestion: "",
    cumulativeSales: "",
    id: "",
  });

  const [activeMarker, setActiveMarker] = useState(null);
  const { isLoaded } = useJsApiLoader({
    id: "google-map-script",
    googleMapsApiKey: "AIzaSyBk9v_3cS5ILPEyEWP69b8L_1erchIdlhU",
  });

  const [map, setMap] = React.useState<any>(null);

  const onLoad = React.useCallback(function callback(map: any) {
    // const bounds = new window.google.maps.LatLngBounds(center);
    // map.fitBounds(bounds);
    setMap(map);
  }, []);

  const onUnmount = React.useCallback(function callback(map: any) {
    setMap(null);
  }, []);

  const handleActiveMarker = (marker: any) => {
    if (marker === activeMarker) {
      console.log(marker);
      return;
    }
    setActiveMarker(marker);
  };

  // ================================================마커데이터

  useEffect(() => {
    const mapApi = async () => {
      const {
        data: { results: mapDb },
      } = await authApi()
        .get("/api/v1/user/getHydrogen")
        .then((res: any) => {
          setToken(res.headers.authorization);
          authApi();
          return res;
        });

      setMapDb(mapDb);
    };
    mapApi();
  }, []);

  // ================================================마커데이터

  return isLoaded ? (
    <>
      <TitleWrap>
        <GraphTitle title={"수소충전소 MAP"} subtitle={""} />
      </TitleWrap>
      <Wrap>
        <GoogleMap
          mapContainerStyle={containerStyle}
          center={center}
          zoom={8}
          onLoad={onLoad}
          onUnmount={onUnmount}
        >
          {mapDb.length > 0
            ? mapDb.map((db: any) => (
                <Marker
                  key={db?.id}
                  position={db?.position}
                  onClick={() => {
                    handleActiveMarker(db?.id);
                  }}
                >
                  {activeMarker === db.id ? (
                    <InfoWindow
                      onCloseClick={() => {
                        setActiveMarker(null);
                      }}
                    >
                      <GoogleMapMarker
                        id={db.id}
                        name={db?.name}
                        number={db?.number}
                        price={db?.price}
                        address={db?.address}
                      />
                    </InfoWindow>
                  ) : (
                    ""
                  )}
                </Marker>
              ))
            : ""}
        </GoogleMap>
      </Wrap>
    </>
  ) : (
    <></>
  );
};
