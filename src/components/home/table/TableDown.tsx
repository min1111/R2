import { Link } from "react-router-dom";
import styled from "styled-components";
import { fileUploadApi } from "../../../api";

const Wrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: right;
  padding-top: 10px;
  margin-top: 30px;
`;
const Button = styled.div`
  font-size: 18px;
  padding: 20px 50px;
  background-color: #001c4d;
  color: white;
  border-radius: 15px;
  font-weight: 600;
`;
export const TableDown = ({ dayPick }: any) => {
  return (
    <Wrap>
      <a
        href={`http://localhost:6112/excel/${dayPick.startvalue}/${dayPick.endvalue}`}
      >
        <Button>다운받기</Button>
      </a>
    </Wrap>
  );
};
