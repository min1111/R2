import { Paper, TableRow } from "@mui/material";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import Table from "@mui/material/Table";
import { useEffect, useState } from "react";
import { setToken } from "../../login/LoginInput";
import { authApi } from "../../../api";

interface TryInfo {
  date: string;
  value: any;
}

export const STable = ({ db }: any) => {
  return (
    <Paper sx={{ width: "100%" }}>
      <TableContainer sx={{ maxHeight: 440 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              <TableCell
                sx={{
                  backgroundColor: " #001c4d",
                  color: "white",
                  fontSize: "18px",
                  borderTopLeftRadius: "10px",
                }}
                align="center"
              >
                날짜
              </TableCell>
              <TableCell
                sx={{
                  backgroundColor: " #001c4d",
                  color: "white",
                  fontSize: "18px",
                }}
                align="center"
              >
                제품명
              </TableCell>
              <TableCell
                sx={{
                  backgroundColor: " #001c4d",
                  color: "white",
                  fontSize: "18px",
                }}
                align="center"
              >
                판매량
              </TableCell>
              <TableCell
                sx={{
                  backgroundColor: " #001c4d",
                  color: "white",
                  fontSize: "18px",
                  borderTopRightRadius: "10px",
                }}
                align="center"
              >
                비고
              </TableCell>
            </TableRow>
          </TableHead>

          <TableBody>
            {db.map((data: any) => (
              <>
                <TableRow key={data.index}>
                  <TableCell align="center" rowSpan={data.value.length + 1}>
                    {data?.date}
                  </TableCell>
                </TableRow>
                {data.value.map((value: any) => (
                  <TableRow key={value.index}>
                    <TableCell align="center">{value.name}</TableCell>
                    <TableCell align="center">{value.quantity}</TableCell>
                    <TableCell align="center"></TableCell>
                  </TableRow>
                ))}
              </>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
};
