import { STable } from "./Table";
import TextField from "@mui/material/TextField";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { useState, useEffect } from "react";
import { setToken } from "../../login/LoginInput";
import { authApi } from "../../../api";
import styled from "styled-components";
import { GraphTitle } from "../graphDashboard/GraphTitle";
import { TableDown } from "./TableDown";

const Section = styled.div`
  padding: 20px;
  background-color: white;
  border-radius: 15px;
`;
const TitleWrap = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
  margin-bottom: 30px;

  /* background-color: white; */
`;

const TitleBox = styled.div`
  background-color: white;

  padding: 10px;
`;

export const TableDashboard = () => {
  const [dayPick, setDayPick] = useState({
    startvalue: "2022-08-01",
    endvalue: "2022-08-31",
  });
  // const [startvalue, setStartValue] = useState<Dayjs | null>(null);
  // const [endvalue, setEndtValue] = useState<Dayjs | null>(null);

  const [db, setDb] = useState([]);

  useEffect(() => {
    const tableDb = async () => {
      if (dayPick.startvalue && dayPick.endvalue) {
        const {
          data: { results: db },
        } = await authApi()
          .get(
            `/api/v1/user/salesList/${dayPick.startvalue}/${dayPick.endvalue}`
          )
          .then((res: any) => {
            setToken(res.headers.authorization);
            authApi();
            return res;
          });
        setDb(db);
      }
    };
    tableDb();
  }, [dayPick]);

  return (
    <Section>
      <TitleWrap>
        <TitleBox>
          <GraphTitle title="일자별 판매수량" subtitle="" />
        </TitleBox>
        <div>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DatePicker
              label="시작일"
              value={dayPick.startvalue}
              onChange={(newValue: any) => {
                const day: any = `${newValue?.$y}-${(
                  "0" +
                  (newValue?.$M + 1)
                ).slice(-2)}-${("0" + newValue?.$D).slice(-2)}`;
                setDayPick({ ...dayPick, startvalue: day });
                console.log(newValue);
              }}
              renderInput={(params: any) => (
                <TextField
                  sx={{
                    border: "1px solid white",
                    backgroundColor: "white",
                    borderRadius: "5px",
                  }}
                  {...params}
                />
              )}
            />
            <DatePicker
              label="종료일"
              value={dayPick.endvalue}
              onChange={(newValue: any) => {
                const day: any = `${newValue?.$y}-${(
                  "0" +
                  (newValue?.$M + 1)
                ).slice(-2)}-${("0" + newValue?.$D).slice(-2)}`;
                setDayPick({ ...dayPick, endvalue: day });
              }}
              renderInput={(params: any) => (
                <TextField
                  sx={{
                    border: "1px solid white",
                    backgroundColor: "white",
                    borderRadius: "5px",
                  }}
                  {...params}
                />
              )}
            />
          </LocalizationProvider>
        </div>
      </TitleWrap>
      <STable db={db} />
      <TableDown dayPick={dayPick} />
    </Section>
  );
};
