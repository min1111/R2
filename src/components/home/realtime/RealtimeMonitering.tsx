import { ResponsiveLine } from "@nivo/line";
import styled from "styled-components";
import { GraphTitle } from "../graphDashboard/GraphTitle";
import { useEffect, useState, useRef } from "react";
import { authApi, Api } from "../../../api";
import { setToken } from "../../login/LoginInput";

import { useSelector, useDispatch } from "react-redux";
import { Loading } from "../../Loading";
import { stompClient } from "../../sock/SockBell";
import { hydrogenStatus } from "../../../_actions/hydrogen_action";

const TitleWrap = styled.div`
  width: 300px;
  margin-top: 50px;
  overflow: hidden;
  background-color: white;
  padding: 20px 0 10px 20px;
  border-top-right-radius: 15px;
  border-top-left-radius: 15px;
`;

const Wrap = styled.div`
  padding: 30px;
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: repeat(20, 1fr);
  grid-template-rows: repeat(8, 1fr);
  column-gap: 1px;
  row-gap: 1px;
`;
const Box = styled.div`
  width: 100%;
  min-height: 30px;
  height: 100%;
  position: relative;
  display: flex;
  align-items: center;
  text-align: center;
  justify-content: center;
  color: black;
  font-size: 12px;
  border-radius: 5px;
  transition: 0.5s;

  &:hover {
    transform: scale(1.2);
    z-index: 5556;
    div {
      display: block;
    }
  }
`;
const NameBox = styled.div`
  width: 150px;
  transition: 0.5s;
  position: absolute;
  font-size: 12px;
  top: -50px;
  left: 0;
  display: none;
  background-color: black;
  color: white;
  padding: 10px;
  border-radius: 5px;
  z-index: 5555;
`;

// const test = [
//     {id:1,
//     name:"서울특별시 서소문청사 수소충전소",
//     status: 3

//     }
// ]
export const RealtimeMonitering = () => {
  // const moniteringDb: any[] = useSelector(
  //   (state: any) => state?.hydrogen.status
  // );
  const box: any = useRef();
  // const dispatch = useDispatch();

  // useEffect(() => {
  //   const test = stompClient.subscribe("/hydrogen/public", onStausRecived);
  //   return () => {
  //     test.unsubscribe();
  //   };
  // }, [box]);

  // const onConnected = () => {};

  // const onStausRecived = (payload: any) => {
  //   let payloadData = JSON.parse(payload.body);
  //   console.log(payloadData);
  //   if (payloadData.body.code === 1) {
  //     dispatch(hydrogenStatus(payloadData.body.results));
  //   }
  // };
  // const onError = (err: any) => {
  //   console.log(err);
  // };

  // const test1 = () => {
  //   stompClient.subscribe("/hydrogen/public", onStausRecived);
  // };
  // const test2 = () => {
  //   stompClient.subscribe("/hydrogen/public", onStausRecived).unsubscribe();
  // };
  // ================================================== 소켓 통신

  const [moniteringDb, setMoniteringDb] = useState<any[]>([]);
  useEffect(() => {
    const dbGet = async () => {
      const {
        data: { results },
      } = await Api.get("/api/v1/user/getHydrogen4");
      setMoniteringDb(results);
    };
    const timer = setInterval(dbGet, 1000);
    return () => {
      clearInterval(timer);
    };
  }, [box]);

  return (
    <>
      <TitleWrap>
        <GraphTitle title={"수소충전소 실시간 모니터링"} subtitle={""} />
        {/* <button onClick={test1}>구독</button>
        <button onClick={test2}>끊기</button> */}
      </TitleWrap>

      <div
        style={{
          width: "100%",
          height: "600px",
          margin: "0 auto",
          backgroundColor: "white",
          borderBottomLeftRadius: "15px",
          borderBottomRightRadius: "15px",
          borderTopRightRadius: "15px",
        }}
        ref={box}
      >
        {moniteringDb?.length > 0 ? (
          <Wrap>
            {moniteringDb.map(
              (db: { id: string; name: string; status: number }) => (
                <Box
                  key={db?.id}
                  style={{
                    backgroundColor: `${
                      db?.status === 0
                        ? "green"
                        : `${
                            db?.status === 1
                              ? "orange"
                              : `${db?.status === 2 ? "red" : "gray"}`
                          }`
                    }`,
                  }}
                >
                  {/* <span>{db?.status === 2 ? db.name : ""}</span> */}
                  <NameBox>{db?.name}</NameBox>
                </Box>
              )
            )}
          </Wrap>
        ) : (
          <Loading />
        )}
      </div>
    </>
  );
};
