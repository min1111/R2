import { RealtimeGraph } from "./RealtimeGraph";
import { RealtimeMonitering } from "./RealtimeMonitering";

export const RealTime = () => {
  return (
    <>
      <RealtimeGraph />
      <RealtimeMonitering />
    </>
  );
};
