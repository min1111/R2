import { ResponsiveLine } from "@nivo/line";
import { GraphTitle } from "../graphDashboard/GraphTitle";
import styled from "styled-components";
import { useState, useEffect } from "react";
import { authApi } from "../../../api";
import { setToken } from "../../login/LoginInput";
import { Loading } from "../../Loading";

const TitleWrap = styled.div`
  width: 300px;
  margin-top: 50px;
  overflow: hidden;
  background-color: white;
  padding: 20px 0 10px 20px;
  border-top-right-radius: 15px;
  border-top-left-radius: 15px;
`;

export const RealtimeGraph = () => {
  const [datas, setDatas] = useState<any[]>([]);
  useEffect(() => {
    const mapApi = async () => {
      const {
        data: { results },
      } = await authApi().get(`/api/v1/user/getHydrogen3`);
      setDatas([results]);
    };
    mapApi();
    setInterval(() => {
      mapApi();
    }, 60000);
  }, []);

  return (
    <>
      {datas.length > 0 && datas !== null ? (
        <>
          <TitleWrap>
            <GraphTitle title={"수소충전소 실시간 이용량"} subtitle={""} />
          </TitleWrap>
          <div
            style={{
              width: "100%",
              height: "400px",
              margin: "0 auto",
              backgroundColor: "white",
              borderBottomLeftRadius: "15px",
              borderBottomRightRadius: "15px",
              borderTopRightRadius: "15px",
            }}
          >
            <ResponsiveLine
              data={datas}
              margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
              xScale={{ type: "point" }}
              yScale={{
                type: "linear",
                min: "auto",
                max: "auto",
                stacked: true,
                reverse: false,
              }}
              yFormat=" >-.2f"
              curve="catmullRom"
              axisTop={null}
              axisRight={null}
              colors={["#002d7d"]}
              lineWidth={2}
              pointSize={10}
              pointColor={["#002d7d"]}
              pointBorderWidth={1}
              pointBorderColor={{ from: "serieColor" }}
              pointLabelYOffset={-12}
              useMesh={true}
              legends={[
                {
                  anchor: "bottom-right",
                  direction: "column",
                  justify: false,
                  translateX: 100,
                  translateY: 0,
                  itemsSpacing: 0,
                  itemDirection: "left-to-right",
                  itemWidth: 80,
                  itemHeight: 20,
                  itemOpacity: 0.75,
                  symbolSize: 12,
                  symbolShape: "circle",
                  symbolBorderColor: "rgba(0, 0, 0, .5)",
                  effects: [
                    {
                      on: "hover",
                      style: {
                        itemBackground: "rgba(0, 0, 0, .03)",
                        itemOpacity: 1,
                      },
                    },
                  ],
                },
              ]}
            />
          </div>
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};
