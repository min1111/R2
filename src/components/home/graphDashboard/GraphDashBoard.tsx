import styled from "styled-components";
import { Linechart } from "../chart/Linechart";
import "react-circular-progressbar/dist/styles.css";
import { Progressbar } from "../chart/Progressbar";
import { GraphTitle } from "./GraphTitle";
import { Barchart2 } from "../chart/Barchart2";
import { SingleSelling } from "./SingleSelling";
import Piechart from "../chart/Piechart";
import { useEffect, useState } from "react";
import { Api, authApi, loginApi } from "../../../api";
import { Loading } from "../../Loading";
import { setToken } from "../../login/LoginInput";
import axios from "axios";
const SingWrap = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 50px;
`;
const ChartWrap = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  grid-template-rows: repeat(2, 1fr);
  grid-row-gap: 10px;
  grid-column-gap: 10px;

  margin-bottom: 50px;
`;
const Chart = styled.div`
  background-color: white;
  border-radius: 5px;
  padding: 40px 40px;
  &:nth-child(1) {
    grid-column: 1 / 3;
  }
  &:nth-child(2) {
    grid-column: 3 / 4;
    grid-row: 1 / 3;
  }
  &:nth-child(3) {
    grid-column: 1 / 2;
    grid-row: 2 / 3;
  }
  &:nth-child(4) {
    grid-column: 2 / 3;
    grid-row: 2 / 3;
  }
`;

export const GraphDashBoard = () => {
  const [daySellingDb, setDaySellingDb] = useState([]);
  const [progressBar, setProgressBar] = useState([]);
  const [bumbDb, setBumpDb] = useState([]);
  const [barDb, setBarDb] = useState([]);
  const [pieDb, setPieDb] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const graphDB = async () => {
      const {
        data: { results: dayDb },
      } = await Api.get("/api/v1/user/daySelling/2022-12-14");
      setDaySellingDb(dayDb);

      const {
        data: { results: progressDb },
      } = await Api.get("/api/v1/user/progressBar/2022-12");
      setProgressBar(progressDb);

      const {
        data: { results: bumpDb },
      } = await Api.get("/api/v1/user/bump/2022-07/2022-12");
      setBumpDb(bumpDb);

      const {
        data: { results: barDb },
      } = await Api.get("/api/v1/user/bar/2022-07/2022-12");
      setBarDb(barDb);

      const {
        data: { results: pieDb },
      } = await Api.get("/api/v1/user/pie/2022-12").then((res: any) => {
        setToken(res.headers.authorization);
        authApi();
        return res;
      });
      setPieDb(pieDb);
      setLoading(true);
    };
    graphDB();
  }, []);

  return (
    <>
      {loading ? (
        <>
          {daySellingDb.length > 0 ? (
            <>
              <SingWrap>
                <SingleSelling
                  bg="linear-gradient(to right, #4f70fe, #c477f7)"
                  daydb={daySellingDb[0]}
                />
                <SingleSelling
                  bg="linear-gradient(to right,  #3DADF6,#757BFC)"
                  daydb={daySellingDb[1]}
                />
                <SingleSelling
                  bg="linear-gradient(to right, #EAA369, #E46C81)"
                  daydb={daySellingDb[2]}
                />
                <SingleSelling
                  bg="linear-gradient(to right, #A6E162, #48D19B)"
                  daydb={daySellingDb[3]}
                />
              </SingWrap>
            </>
          ) : (
            <Loading />
          )}

          <ChartWrap>
            {bumbDb.length > 0 ? (
              <Chart>
                <GraphTitle
                  title="월별 판매 랭킹"
                  subtitle="2022년 12월 기준"
                />
                <Linechart bumpDb={bumbDb} />
              </Chart>
            ) : (
              <Loading />
            )}

            {progressBar.length > 0 ? (
              <Chart>
                <GraphTitle
                  title="전년 대비 매출수량 달성률"
                  subtitle="2022년 12월 기준"
                />
                <Progressbar progressDB={progressBar[0]} color="#c477f7" />
                <Progressbar progressDB={progressBar[1]} color="#757BFC" />
                <Progressbar progressDB={progressBar[2]} color="#E46C81" />
                <Progressbar progressDB={progressBar[3]} color="#48D19B" />
              </Chart>
            ) : (
              <Loading />
            )}

            {barDb.length > 0 ? (
              <Chart>
                <GraphTitle
                  title="월별 판매 수량"
                  subtitle="2022년 12월 기준"
                />
                <Barchart2 barDb={barDb} />
              </Chart>
            ) : (
              <Loading />
            )}

            {pieDb.length > 0 ? (
              <Chart>
                <GraphTitle
                  title="월별 판매 점유율"
                  subtitle="2022년 12월 기준"
                />
                <Piechart pieDb={pieDb} />
              </Chart>
            ) : (
              <Loading />
            )}
          </ChartWrap>
        </>
      ) : (
        <Loading />
      )}
    </>
  );
};
