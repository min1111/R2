import styled from "styled-components";

const Title = styled.div`
  font-size: 24px;
  font-weight: 500;
  margin-bottom: 15px;
`;
const SubTitle = styled.div`
  font-size: 16px;
  font-weight: 500;
  opacity: 0.3;
`;

interface Props {
  title: string;
  subtitle: string;
}

export const GraphTitle = ({ title, subtitle }: Props) => {
  return (
    <>
      <Title>{title}</Title>
      <SubTitle> {subtitle} </SubTitle>
    </>
  );
};
