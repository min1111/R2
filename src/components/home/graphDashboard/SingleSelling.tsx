import styled from "styled-components";
import { Loading } from "../../Loading";

const Wrap = styled.div`
  width: 24%;
  height: 140px;
  padding: 15px 20px;
  box-shadow: rgba(50, 50, 93, 0.25) 0px 13px 27px -5px,
    rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;
`;
const Title = styled.div`
  font-size: 18px;
  font-weight: 400;
  color: white;
  margin-bottom: 15px;
`;
const SellCoung = styled.div`
  font-size: 40px;
  font-weight: 600;
  color: white;
  margin-bottom: 15px;
  span {
    font-size: 16px;
    font-weight: 400;
    margin-left: 5px;
  }
`;
const GageCon = styled.div`
  width: 100%;
  height: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const GageWrap = styled.div`
  width: 80%;
  height: 10px;
`;
const GageBg = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 5px;
  background-color: rgba(255, 255, 255, 0.2);
`;
const Gage = styled.div`
  height: 100%;
  border-radius: 5px;
  background: linear-gradient(
    to right,
    rgba(255, 255, 255, 0),
    rgba(255, 255, 255, 1)
  );
`;
const GageCount = styled.div`
  width: 20%;
  color: white;
  font-size: 18px;
  text-align: right;
`;

interface Props {
  bg: string;
  daydb: { date: string; name: string; quantity: number };
}

export const SingleSelling = ({ bg, daydb }: Props) => {
  return (
    <Wrap style={{ background: bg }}>
      {daydb ? (
        <>
          <Title> {daydb.name} </Title>
          <SellCoung>
            {daydb.quantity} <span>개</span>
          </SellCoung>
          <GageCon>
            <GageWrap>
              <GageBg>
                <Gage
                  style={{ width: `${Math.ceil(daydb.quantity / 100)}%` }}
                />
              </GageBg>
            </GageWrap>
            <GageCount>{Math.ceil(daydb.quantity / 100)}%</GageCount>
          </GageCon>
        </>
      ) : (
        <Loading />
      )}
    </Wrap>
  );
};
