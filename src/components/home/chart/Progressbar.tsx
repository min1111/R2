// Import react-circular-progressbar module and styles
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import styled from "styled-components";

// Animation

const Wrap = styled.div`
  margin-top: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Title = styled.div`
  font-size: 28px;
  font-weight: 600;
  color: #4f78d3;
  margin-bottom: 15px;
`;
const Count = styled.div`
  font-size: 24px;
  font-weight: 500;
  span {
    font-size: 16px;
    font-weight: 100;
    opacity: 0.7;
  }
`;
const TextWrap = styled.div`
  width: 50%;
  margin-left: 40px;
`;

interface Props {
  color: string;
  progressDB: {
    date: string;
    id: string;
    quantity: number;
    ratio: any;
    targetValue: number;
  };
}

export const Progressbar = ({ progressDB, color }: Props) => {
  const percent = Math.floor(
    (progressDB.quantity / progressDB.targetValue) * 100
  );
  return (
    <Wrap>
      <div style={{ width: "200px", maxHeight: "180px" }}>
        <CircularProgressbar
          value={percent}
          text={`${percent} %`}
          styles={buildStyles({
            textSize: "16px",

            pathColor: `${color}`,
            textColor: `${color}`,
            trailColor: "rgba(0,0,0,0.07)",
            backgroundColor: "black",
          })}
        />
      </div>
      <TextWrap>
        <Title style={{ color: `${color}` }}>{progressDB.id}</Title>
        <Count>
          {progressDB.quantity}개 / <br />
          <span>{progressDB.targetValue}개</span>
        </Count>
      </TextWrap>
    </Wrap>
  );
};
