import styled from "styled-components";
import { GraphDashBoard } from "./graphDashboard/GraphDashBoard";
import { TableDashboard } from "./table/TableDashboard";
import { GooleMap } from "./gooleMap/GoogleMap";
import { RealTime } from "./realtime/RealTime";

const Section = styled.section`
  padding: 120px 120px 0 120px;
`;

export const Home = () => {
  return (
    <Section>
      <GraphDashBoard />
      <TableDashboard />
      <GooleMap />
      <RealTime />
    </Section>
  );
};
