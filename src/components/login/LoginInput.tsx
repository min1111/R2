import { faGoogle } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect } from "react";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { loginUser } from "../../_actions/user_action";
import { authApi, loginApi } from "../../api";
import { useCookies } from "react-cookie";
import { useNavigate } from "react-router-dom";

const Wrap = styled.div`
  width: 350px;
  height: 630px;
  display: flex;
  flex-direction: column;
  padding: 70px 35px 25px 35px;
  background-color: #f6f8fa;
`;
const Title = styled.div`
  font-size: 32px;
  color: #0d3d90;
  font-weight: 400;
  span {
    font-weight: 900;
    margin-right: 10px;
  }
  margin-bottom: 40px;
`;
const GoogleButton = styled.div`
  width: 100%;
  height: 35px;
  background-color: #ffffff;
  display: flex;
  position: relative;
  align-items: center;
  justify-content: center;
  margin-bottom: 40px;
  border-radius: 5px;
  box-shadow: rgba(0, 0, 0, 0.04) 0px 3px 5px;
  cursor: pointer;
  span {
    color: #afb8c5;
    font-weight: 500;
  }
`;

const Icon = styled.div`
  position: absolute;
  left: 10px;
  font-size: 18px;
`;
const Bar = styled.div`
  font-size: 14px;
  font-weight: 700;
  color: #707070;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 40px;
  &::before {
    content: "";
    position: relative;
    width: 100px;
    height: 1px;
    border-bottom: solid 1px black;
  }
  &::after {
    content: "";
    position: relative;
    width: 100px;
    height: 1px;
    border-bottom: solid 1px black;
  }
`;
const Label = styled.div`
  font-size: 16px;
  font-weight: 600;
  color: #0d3d90;
  margin-bottom: 10px;
`;
const Input = styled.input`
  all: unset;
  width: 100%;
  height: 35px;
  background-color: #ffffff;
  border: solid 1px #eff1f3;
  border-radius: 5px;
  margin-bottom: 20px;
  box-sizing: border-box;
  font-size: 14px;
  padding: 0 15px;
  box-shadow: rgba(0, 0, 0, 0.04) 0px 3px 5px;
  &::placeholder {
    color: #b2bdce;
  }

  &:focus {
    color: #b2bdce;
    font-weight: 600;
  }
`;
const Button = styled.button`
  all: unset;
  width: 100%;
  height: 40px;
  border-radius: 5px;
  background-color: #fa6676;
  border: solid 1px #fb5667;
  color: #fccccf;
  font-size: 14px;
  font-weight: 700;
  text-align: center;
  margin-bottom: 45px;
  box-shadow: rgba(0, 0, 0, 0.04) 0px 3px 5px;
  cursor: pointer;
`;
const Forgot = styled.div`
  font-size: 12px;
  color: #bdc5d7;
  font-weight: 600;
  text-align: center;
  letter-spacing: 0;
  margin-bottom: 60px;
  cursor: pointer;
`;
const EndLineWrap = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`;
const SignWrap = styled.div`
  font-size: 11px;
  font-weight: 400;
  color: #bdc5d7;
  span {
    font-size: 13px;
    color: #0d3d90;
    font-weight: 900;
    margin-left: 10px;
    cursor: pointer;
  }
`;
const Logo = styled.div`
  font-size: 18px;
  color: #0d3d90;
  font-weight: 900;
  cursor: pointer;
`;

export interface loginValue {
  email: string;
  password: number;
}

export const setToken = (data: string) => {
  localStorage.setItem("authorization", data);
  authApi();
};

export const LoginInPUt = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { register, handleSubmit, getValues } = useForm<loginValue>({
    mode: "onChange",
  });

  const submit = () => {
    const loginDb = getValues();
    dispatch(loginUser(loginDb)).then((res: any) => {
      if (res.payload.data.code !== 1) {
        console.log(res);
      } else {
        console.log(res);
        setToken(res.payload.headers.authorization);
        navigate("/home");
      }
    });
  };

  return (
    <Wrap>
      <Title>
        <span>Log</span>in
      </Title>
      <GoogleButton>
        <Icon>
          <FontAwesomeIcon icon={faGoogle} />
        </Icon>
        <span>Use Goole Account</span>
      </GoogleButton>
      <Bar>or</Bar>
      <form onSubmit={handleSubmit(submit)}>
        <Label>E-mail</Label>
        <Input
          type="text"
          placeholder="pacom@ipacom.com"
          {...register("email")}
        ></Input>
        <Label>Password</Label>
        <Input
          type="password"
          placeholder="•••••"
          {...register("password")}
        ></Input>
        <Button>Log In</Button>
      </form>

      <Forgot>Forgot Password?</Forgot>
      <EndLineWrap>
        <SignWrap>
          Don't have on account?
          <span>Sign Up</span>
        </SignWrap>
        <Logo>Pacom</Logo>
      </EndLineWrap>
    </Wrap>
  );
};
