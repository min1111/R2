import styled from "styled-components";
import { LoginInPUt } from "./LoginInput";

const Section = styled.section`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const Wrap = styled.div`
  width: 1000px;
  height: 630px;
  display: flex;
`;

const BannerImg = styled.img`
  width: 650px;
  height: 100%;
`;

export const Login = () => {
  return (
    <Section>
      <Wrap>
        <BannerImg src="/img/loginbanner.png " />
        <LoginInPUt />
      </Wrap>
    </Section>
  );
};
