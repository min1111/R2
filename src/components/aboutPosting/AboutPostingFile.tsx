import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileExcel } from "@fortawesome/free-solid-svg-icons";
import { useState, useEffect } from "react";
import { Controller, useFormContext } from "react-hook-form";
import { faYoutube } from "@fortawesome/free-brands-svg-icons";

const ImgWrap = styled.div`
  width: 100%;
  height: 100px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 20px;
`;

const Box = styled.div`
  width: 45%;
  height: 100%;
  border: 5px dashed rgb(13, 61, 144);
  border-radius: 25px;
  background-size: 10px 2px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  overflow: hidden;
`;

const Text = styled.div`
  font-size: 28px;
  color: white;
`;
const Input = styled.input`
  display: none;
`;

const Img = styled.div`
  width: 100%;
  height: 100%;
`;

const ShowImg = styled.div`
  display: flex;
  overflow: hidden;
  width: 100%;
  height: 100%;
`;

export const AboutPostingFile = ({}: any) => {
  const { control, watch, register } = useFormContext();

  const [videofileName, setVideoFileName] = useState([]);
  const [textfileName, setTextFileName] = useState([]);

  const videoPreview = watch("video");
  const textPreview = watch("textfile");

  useEffect(() => {
    if (videoPreview && videoPreview.length > 0) {
      // ====================================================
      let nameList: any = [];
      for (let i: number = 0; i < videoPreview.length; i++) {
        nameList.push(videoPreview[i].name);
      }
      setVideoFileName(nameList);
      //===========================================비디오 이름 저장
    }
  }, [videoPreview]);

  useEffect(() => {
    if (textPreview && textPreview.length > 0) {
      // ====================================================
      let nameList: any = [];
      for (let i: number = 0; i < textPreview.length; i++) {
        nameList.push(textPreview[i].name);
      }
      setTextFileName(nameList);
      //=========================================== 문서 이름 저장
    }
  }, [textPreview]);

  const deleteImgFile = () => {};
  return (
    <ImgWrap>
      <Box>
        <Controller
          control={control}
          name="video"
          render={({}) => {
            return (
              <Input
                type="file"
                accept="video/*"
                id="profileVideo"
                {...register("video")}
              />
            );
          }}
        />

        <label onClick={deleteImgFile} htmlFor="profileVideo">
          {videofileName.length > 0 ? (
            <>
              {videofileName.map((name) => (
                <p
                  key={name}
                  style={{ color: "rgb(13, 61, 144)", fontSize: "16px" }}
                >
                  파일명 : {name}
                </p>
              ))}
            </>
          ) : (
            <FontAwesomeIcon
              style={{ fontSize: "40px", color: "red" }}
              icon={faYoutube}
            />
          )}
        </label>
      </Box>

      <Box>
        <Controller
          control={control}
          name="textfile"
          render={({}) => {
            return (
              <Input
                type="file"
                accept="text/plain"
                id="profilefile"
                {...register("textfile")}
              />
            );
          }}
        />

        <label onClick={deleteImgFile} htmlFor="profilefile">
          {textfileName.length > 0 ? (
            <>
              {textfileName.map((name) => (
                <p
                  key={name}
                  style={{ color: "rgb(13, 61, 144)", fontSize: "16px" }}
                >
                  파일명 : {name}
                </p>
              ))}
            </>
          ) : (
            <FontAwesomeIcon
              style={{ fontSize: "40px", color: "green" }}
              icon={faFileExcel}
            />
          )}
        </label>
      </Box>
    </ImgWrap>
  );
};
