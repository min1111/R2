import { Controller, useFormContext } from "react-hook-form";

import styled from "styled-components";

import { useState } from "react";
import { AboutPostingFile } from "./AboutPostingFile";

const TextWrap = styled.div`
  width: 100%;

  display: flex;
  flex-direction: column;
  padding: 60px 35px 25px 35px;
  background-color: #f6f8fa;
  border-radius: 25px;
`;

const RadioWrap = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 50px;
  margin-bottom: 20px;
`;

const Radio = styled.input`
  display: none;
`;

const RadioLabel = styled.label`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: 50%;
  border: 1px solid #eff1f3;
  color: #b2bdce;
`;

const Label = styled.div`
  font-size: 20px;
  font-weight: 600;
  color: #0d3d90;
  margin-bottom: 20px;
`;
const Input = styled.input`
  all: unset;
  width: 100%;
  height: 50px;
  background-color: #ffffff;
  border: solid 1px #eff1f3;
  border-radius: 5px;
  margin-bottom: 20px;
  box-sizing: border-box;
  font-size: 14px;
  padding: 0 15px;
  box-shadow: rgba(0, 0, 0, 0.04) 0px 3px 5px;
  &::placeholder {
    color: #b2bdce;
  }

  &:focus {
    color: #b2bdce;
    font-weight: 600;
  }
`;
const TextArea = styled.textarea`
  all: unset;
  width: 100%;
  height: 50px;
  background-color: #ffffff;
  border: solid 1px #eff1f3;
  border-radius: 5px;
  margin-bottom: 20px;
  box-sizing: border-box;
  font-size: 14px;
  padding: 15px 15px;
  box-shadow: rgba(0, 0, 0, 0.04) 0px 3px 5px;
  height: 300px;

  &::placeholder {
    color: #b2bdce;
  }

  &:focus {
    color: #b2bdce;
    font-weight: 600;
  }
`;
const Button = styled.button`
  all: unset;
  width: 100%;
  height: 40px;
  border-radius: 5px;
  background-color: #fa6676;
  border: solid 1px #fb5667;
  color: #fccccf;
  font-size: 14px;
  font-weight: 700;
  text-align: center;
  margin-bottom: 45px;
  box-shadow: rgba(0, 0, 0, 0.04) 0px 3px 5px;
  cursor: pointer;
`;

export const AboutPostingText = () => {
  const { control, register } = useFormContext();
  const [focus, setFocus] = useState("report");
  const focusStyle = {
    backgroundColor: "#0d3d90",
    color: "white",
    fontWeight: "600",
  };

  return (
    <>
      <TextWrap>
        <RadioWrap>
          <Radio
            type={"radio"}
            id="report"
            value={"보도자료"}
            defaultChecked
            {...register("classification")}
          ></Radio>
          <RadioLabel
            style={focus === "report" ? focusStyle : {}}
            onClick={() => {
              setFocus("report");
            }}
            htmlFor="report"
          >
            보도자료
          </RadioLabel>
          <Radio
            {...register("classification")}
            type={"radio"}
            id="media"
            value={"미디어행사"}
          ></Radio>
          <RadioLabel
            style={focus === "media" ? focusStyle : {}}
            onClick={() => {
              setFocus("media");
            }}
            htmlFor="media"
          >
            미디어행사
          </RadioLabel>
        </RadioWrap>
        <Label>Title</Label>
        <Controller
          control={control}
          name="title"
          render={({}) => {
            return (
              <Input
                type="text"
                placeholder="제목을 입력해 주세요"
                {...register("title")}
              />
            );
          }}
        />

        <Label>Main</Label>

        <Controller
          control={control}
          name="content"
          render={({}) => {
            return (
              <TextArea
                cols={80}
                rows={20}
                placeholder="본문내용을 작성해 주세요"
                {...register("content")}
              />
            );
          }}
        />
        <AboutPostingFile />
        <Button>글 쓰 기</Button>
      </TextWrap>
    </>
  );
};
