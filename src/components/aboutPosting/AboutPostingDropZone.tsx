import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";
import { useState, useEffect } from "react";
import { Controller, useForm, useFormContext } from "react-hook-form";

const ImgWrap = styled.div`
  width: 100%;
  height: 500px;
  padding: 50px 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Box = styled.div`
  width: 45%;
  height: 100%;
  border: 5px dashed #f6f8fa;
  border-radius: 25px;
  background-size: 10px 2px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  overflow: hidden;
  span {
    width: 75px;
    height: 75px;
    border-radius: 50%;
    font-size: 30px;
    border-radius: 900;
    background-color: white;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const Text = styled.div`
  font-size: 28px;
  color: white;
`;
const Input = styled.input`
  display: none;
`;

const Img = styled.div`
  width: 100%;
  height: 100%;
`;

const ShowImg = styled.div`
  display: flex;
  overflow: hidden;
  width: 100%;
  height: 100%;
`;

export const AboutPostingDropZone = ({}: any) => {
  const { control, watch, register } = useFormContext();
  const [img, setImg] = useState([]);
  const [fileName, setFileName] = useState([]);

  const imagePreview = watch("file");

  useEffect(() => {
    if (imagePreview && imagePreview.length > 0) {
      // ====================================================
      let nameList: any = [];
      for (let i: number = 0; i < imagePreview.length; i++) {
        nameList.push(imagePreview[i].name);
      }
      setFileName(nameList);
      //===========================================사진 이름 저장

      let imgList: any = [];
      for (let i: number = 0; i < imagePreview.length; i++) {
        const file: any = imagePreview[i];
        imgList.push(URL.createObjectURL(file));
      }
      setImg(imgList);

      console.log(img);
    }
  }, [imagePreview]);

  const deleteImgFile = () => {
    setImg([]);
  };
  return (
    <ImgWrap>
      <Box>
        <Controller
          control={control}
          name="file"
          render={({}) => {
            return (
              <Input
                type="file"
                accept="image/*"
                id="profileImg"
                multiple
                {...register("file")}
              />
            );
          }}
        />

        <label onClick={deleteImgFile} htmlFor="profileImg">
          <span>
            {img.length > 0 ? (
              <FontAwesomeIcon icon={faMinus} />
            ) : (
              <FontAwesomeIcon icon={faPlus} />
            )}
          </span>
        </label>

        {img.length > 0 ? (
          <>
            {fileName.map((name) => (
              <p style={{ color: "white", marginTop: "30px" }}>
                파일명 : {name}
              </p>
            ))}
          </>
        ) : (
          ""
        )}
      </Box>

      <Box>
        {img.length > 0 ? (
          <ShowImg>
            {img.map((imgData) => (
              <Img
                style={{
                  background: `url(${
                    imgData ? imgData : ""
                  })  no-repeat center/cover`,
                }}
              />
            ))}
          </ShowImg>
        ) : (
          <Text>파일을 업로드 해주세요.</Text>
        )}
      </Box>
    </ImgWrap>
  );
};
