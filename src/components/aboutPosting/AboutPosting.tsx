import { FormProvider, useForm } from "react-hook-form";
import styled from "styled-components";
import { fileUploadApi } from "../../api";
import { setToken } from "../login/LoginInput";
import { AboutPostingDropZone } from "./AboutPostingDropZone";
import { AboutPostingText } from "./AboutPostingText";

import { useNavigate } from "react-router-dom";

const Test = styled.input`
  all: unset;
`;
const Section = styled.section`
  padding: 220px 220px 0 220px;
`;

interface formValue {
  title: string;
  content: string;
  img: any;
}

export const AboutPosting = () => {
  const navigate = useNavigate();
  const methods = useForm({
    mode: "onSubmit",
  });

  const submit = async () => {
    const { classification, title, content, file, video, textfile } =
      methods.getValues();

    console.log(methods.getValues());

    let formData = new FormData();
    formData.append("title", title);
    formData.append("content", content);
    formData.append("video", video[0]);
    formData.append("file", textfile[0]);
    formData.append("classification", classification);

    for (let i: number = 0; i < file.length; i++) {
      formData.append("img", file[i]);
    }
    // file.map((db: any) => {
    //   formData.append("file", db);
    //   console.log(file);
    // });

    const Db: any = await fileUploadApi
      .post("/api/v1/user/posting", formData, {
        headers: {
          Authorization: `${
            localStorage.getItem("authorization")
              ? localStorage.getItem("authorization")
              : "Bearer"
          }`,
        },
      })
      .then((res: any) => {
        setToken(res.headers.authorization);
        return res;
      })
      .catch((err) => console.log(err));

    console.log(Db);

    if (Db.data.code === 1) {
      navigate("/about");
    } else {
    }
  };

  return (
    <Section>
      <FormProvider {...methods}>
        <form
          onSubmit={methods.handleSubmit(submit)}
          encType={"multipart/form-data"}
        >
          <AboutPostingDropZone />

          <AboutPostingText />
        </form>
      </FormProvider>
    </Section>
  );
};
