import {
  faFacebook,
  faTwitter,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
import { faLink } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import styled from "styled-components";
import { authApi } from "../../api";
import { Loading } from "../Loading";
import { setToken } from "../login/LoginInput";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay } from "swiper";
import "swiper/css";
SwiperCore.use([Autoplay]);

const Section = styled.section`
  padding-top: 200px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;
const Classification = styled.h3`
  font-size: 28px;
  opacity: 0.7;
  margin-bottom: 30px;
  font-weight: 400;
`;
const Title = styled.h1`
  max-width: 50%;
  font-size: 42px;
  text-align: center;
  margin-bottom: 50px;
  line-height: 60px;
  font-weight: 600;
`;
const IconWrap = styled.div`
  margin-bottom: 75px;
`;
const Icon = styled.div`
  display: inline-block;
  padding: 10px;
  font-size: 24px;
  background-color: #fff;
  border-radius: 50%;
  margin-right: 15px;
  cursor: pointer;
  &:last-child {
    margin-right: 0;
  }
  &:hover {
    background-color: black;
    color: white;
  }
`;

const DownWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: right;
  margin-bottom: 20px;
`;
const ImgWrap = styled.div`
  width: 100%;
  margin-bottom: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  img {
    width: 100%;
  }
`;
const Img = styled.img`
  width: 100%;
`;
const Text = styled.p`
  font-size: 18px;
  line-height: 30px;
  align-items: center;
  max-width: 50%;
  font-weight: 300;
  margin-bottom: 100px;
  span {
    font-weight: 600;
  }
`;
const Button = styled.div`
  a {
    display: block;
    font-size: 18px;
    font-weight: 500;
    padding: 20px 50px;
    border-radius: 30px;
    background-color: #eee;
  }
`;

interface detailDbType {
  classification?: string;
  content?: string;
  id?: number;
  imgs?: any;
  title?: string;
  userId?: number;
  videos?: any;
  files?: any;
}

export const AboutDetail = () => {
  const [detailDb, setDetailDb] = useState<detailDbType>({});
  const [loading, setLoading] = useState(false);
  const { id } = useParams();
  useEffect(() => {
    const graphDB = async () => {
      const {
        data: { result: detailDb },
      } = await authApi()
        .get(`/api/v1/user/board/${id}`)
        .then((res: any) => {
          setToken(res.headers.authorization);
          authApi();
          return res;
        });
      setDetailDb(detailDb);
      setLoading(true);
    };
    graphDB();
  }, []);
  console.log(detailDb);
  return (
    <Section>
      {loading ? (
        <>
          <Classification>
            {detailDb.classification} | 2022년 12월 15일
          </Classification>
          <Title>{detailDb.title}</Title>
          <IconWrap>
            <Icon>
              <FontAwesomeIcon icon={faTwitter} />
            </Icon>
            <Icon>
              <FontAwesomeIcon icon={faFacebook} />
            </Icon>
            <Icon>
              <FontAwesomeIcon icon={faYoutube} />
            </Icon>
            <Icon>
              <FontAwesomeIcon icon={faLink} />
            </Icon>
          </IconWrap>
          {detailDb?.files.length > 0 ? (
            <DownWrap>
              <a href={`http://localhost:6112/img/${detailDb?.files[0]?.name}`}>
                <div>첨부파일 : {detailDb?.files[0]?.name}</div>
              </a>
            </DownWrap>
          ) : (
            ""
          )}

          <ImgWrap>
            <Swiper
              loop={detailDb?.imgs?.length > 0}
              speed={1000}
              spaceBetween={0}
              slidesPerView={1}
              autoplay={{ delay: 1000 }}
              navigation
            >
              {detailDb.imgs.map((img: any) => (
                <SwiperSlide key={img.name}>
                  <img
                    style={{
                      width: "100%",
                    }}
                    src={`http://localhost:6112/img/${img.name}`}
                  />
                </SwiperSlide>
              ))}
            </Swiper>
          </ImgWrap>
          <Text>
            <span>
              - 본사와 제작 자회사간 스튜디오 체계 구축해 안정적 시너지, 올해
              다양한 플랫폼 통해 총 15편 선보여
              <br /> - '사내맞선' '헌트' '수리남'…드라마, 영화, OTT 시리즈 등
              웰메이드 글로벌 메가 히트작 쏟아내는 흥행 성과
              <br /> - 독창적 크리에이티브와 탄탄한 제작 역량 바탕으로 프리미엄
              콘텐츠IP 확보, 글로벌 스튜디오 경쟁력 입증 <br />- 2023년
              ‘경성크리처’ ‘최악의 악’ 등 기대작 라인업으로 전세계 정조준,
              글로벌 스튜디오 위상 강화
            </span>
            <br />
            <br />
            [2022-12-15] 카카오엔터테인먼트(대표 김성수 이진수)가 2022년
            전세계를 사로잡는 웰메이드 메가 히트작을 대거 선보이며 글로벌
            스튜디오로서 입지를 다졌다. 독창적 크리에이티브와 탁월한 제작 역량을
            갖춘 제작 자회사들과 글로벌 플랫폼 네트워크, 작품 기획 개발, 사업화
            역량 등 콘텐츠 비즈니스 노하우를 보유한 카카오엔터 본사가 유기적으로
            결합해 안정적인 스튜디오 체제를 갖추고, 시너지를 창출해낸 것. 올해
            카카오엔터가 글로벌 OTT를 비롯해 TV, 스크린 등에서 선보인 드라마,
            영화는 총 15편. ‘사내맞선’, ‘브로커’, ‘헌트’, ‘수리남’, ‘종이의 집:
            공동경제구역’ 등 로맨스코미디부터 휴먼 드라마, 액션, 범죄 스릴러에
            이르기까지 다채로운 소재와 장르로 라인업을 꽉 채웠다. 달콤한 감성을
            듬뿍 담은 로맨스코미디로 K로맨스 열풍을 다시금 일으키는가 하면,
            새로운 소재와 탄탄한 스토리텔링, 감각적인 연출로 전세계를 사로잡으며
            작품성과 대중성을 모두 인정받았다. 뿐 아니라 국내외에서 인기를 얻은
            슈퍼IP를 새롭게 재해석, 세계관 확장의 성공 사례를 증명하는 등<br />
            <br /> 올해 카카오엔터는 글로벌 스튜디오로서 확실하게 입지를 다졌다.
            카카오엔터테인먼트는 2023년에도 글로벌 경쟁력을 갖춘 프리미엄
            콘텐츠를 바탕으로, 전세계를 휩쓸고 있는 K콘텐츠 열풍을 이끌 대표
            기업으로서 글로벌 엔터테인먼트 산업 내 영향력을 더욱 강화하겠다는
            계획이다.
            {detailDb.content}
          </Text>
          {detailDb?.videos[0]?.name ? (
            <video style={{ marginBottom: "100px" }} controls width="100%">
              <source
                src={`http://localhost:6112/img/${detailDb?.videos[0]?.name}`}
                type="video/mp4"
              ></source>
              <a
                href={`http://localhost:6112/img/${detailDb?.videos[0]?.name}`}
              >
                MP4
              </a>
            </video>
          ) : (
            ""
          )}

          <Button>
            <Link to={"/about"}>목록 보기</Link>
          </Button>
        </>
      ) : (
        <Loading />
      )}
    </Section>
  );
};
