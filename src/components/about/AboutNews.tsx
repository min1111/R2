import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";
import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay } from "swiper";
import "swiper/css";
SwiperCore.use([Autoplay]);

const Section = styled.section`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  column-gap: 30px;
  row-gap: 60px;
`;

const Con = styled.div`
  width: 100%;
  min-width: 430px;
  height: 640px;
  transition: 0.2s;
  &:hover {
    transform: translateY(-10px) translateX(-5px);
    box-shadow: rgba(0, 0, 0, 0.2) 0px 18px 50px -10px;
  }
`;
const ConWrap = styled.div`
  width: 100%;
  background-color: white;
  border-top-left-radius: 25px;
  border-top-right-radius: 25px;
  padding: 20px 30px;
`;
const Icon = styled.div`
  display: inline-block;
  font-size: 25px;
  color: #fdde1b;
`;
const Division = styled.div`
  display: inline-block;
  width: 75px;
  height: 35px;
  font-size: 13px;
  font-weight: 300;
  text-align: center;
  line-height: 26px;
  border-radius: 17.5px;
  background-color: #eeeeee;
  margin-left: 10px;
`;
const Date = styled.div`
  display: inline-block;
  font-size: 13px;
  font-weight: 300;
  margin-left: 10px;
`;
const Title = styled.div`
  font-size: 28px;
  line-height: 40px;
  font-weight: 600;
  height: 80px;
  margin-top: 22px;
  margin-bottom: 32px;
`;
const Tag = styled.div`
  display: inline-block;
  font-size: 14px;
  font-weight: 300;
  opacity: 0.6;
  margin-right: 10px;
`;
const Img = styled.div`
  width: 100%;
  height: 410px;
  border-bottom-left-radius: 25px;
  border-bottom-right-radius: 25px;
  background-color: #333;
  overflow: hidden;
`;

export const AboutNews = ({ array }: any) => {
  return (
    <Section>
      {array.map((data: any) => (
        <Con key={data.id}>
          <Link to={`/aboutDetail/${data.id}`}>
            <ConWrap>
              <Icon>
                <FontAwesomeIcon icon={faPaperPlane} />
              </Icon>
              <Division>{data.classification} </Division>
              <Date>2022. 12. 15</Date>
              <Title>
                {data.title.slice(0, 35) +
                  `${data.title.length > 35 ? "..." : ""}`}
              </Title>
              <Tag>#Pacom엔터테인먼트</Tag>
              <Tag>#프리미엄IP라인업</Tag>
            </ConWrap>

            <Img>
              <Swiper
                loop={data.imgs.length > 1}
                speed={1000}
                spaceBetween={0}
                slidesPerView={1}
                autoplay={{ delay: 2000 }}
                navigation
              >
                {data.imgs.map((img: any) => (
                  <SwiperSlide key={img.name}>
                    <div
                      style={{
                        minWidth: "430px",
                        width: "100%",
                        height: "410px",
                        background: `url(http://localhost:6112/img/${img.name}) no-repeat center/cover`,
                      }}
                      // src={`http://localhost:6112/img/${img.name}`}
                    />
                  </SwiperSlide>
                ))}
              </Swiper>
            </Img>
          </Link>
        </Con>
      ))}
    </Section>
  );
};
