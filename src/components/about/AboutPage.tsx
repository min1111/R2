import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";

const Wrap = styled.div`
  margin-top: 100px;
  width: 100%;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
`;
const Arrow = styled.div`
  width: 50px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  color: #949494;
  border-radius: 50%;
  background-color: #eeeeee;
  cursor: pointer;
  &:first-child {
    margin-right: 60px;
  }
  &:last-child {
    margin-left: 60px;
  }
`;
const Num = styled.div`
  font-size: 24px;
  font-weight: 300;
  color: #1d1d1d;
  margin: 0 20px;
  cursor: pointer;
`;
const FirstPage = styled.div``;
const MiddlePage = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
const LastPage = styled.div``;

export const AboutPage = ({ focusNum, click, totalPage }: any) => {
  const maxPage = totalPage;
  const dot = () => {
    if (focusNum === 1 || focusNum === 2 || focusNum === 3) {
      return "front";
    } else if (
      focusNum === maxPage ||
      focusNum === maxPage - 1 ||
      focusNum === maxPage - 2
    ) {
      return "back";
    } else {
      return "none";
    }
  };

  const numClick = (e: any) => {
    const element = Number(e.target.innerHTML);
    click(Number(element));
  };

  const leftarrowClick = () => {
    if (focusNum !== 1) {
      click(focusNum - 1);
    }
  };

  const rightarrowClick = () => {
    if (focusNum !== maxPage) {
      click(focusNum + 1);
    }
  };

  const middleNumbers = () => {
    if (focusNum === 1 || focusNum === 2) {
      return 3;
    } else if (focusNum + 1 === maxPage || focusNum === maxPage) {
      return maxPage - 2;
    } else {
      return focusNum;
    }
  };

  return (
    <Wrap>
      <Arrow onClick={leftarrowClick}>
        <FontAwesomeIcon icon={faChevronLeft} />
      </Arrow>
      <FirstPage>
        <Num
          onClick={numClick}
          style={{ fontWeight: focusNum === 1 ? "600" : "300" }}
        >
          1
        </Num>
      </FirstPage>

      {dot() === "front" ? "" : <Num>...</Num>}

      <MiddlePage>
        <Num
          style={{ fontWeight: focusNum === 2 ? "600" : "300" }}
          onClick={numClick}
        >{`${Number(middleNumbers()) - 1} `}</Num>

        <Num
          style={{
            fontWeight:
              focusNum !== 1 &&
              focusNum !== 2 &&
              focusNum !== maxPage &&
              focusNum !== maxPage - 1
                ? "600"
                : "300",
          }}
          onClick={numClick}
        >
          {middleNumbers()}
        </Num>

        <Num
          style={{ fontWeight: focusNum === maxPage - 1 ? "600" : "300" }}
          onClick={numClick}
        >{`${Number(middleNumbers()) + 1} `}</Num>
      </MiddlePage>
      {dot() === "back" ? "" : <Num>...</Num>}
      <LastPage>
        <Num
          onClick={numClick}
          style={{
            fontWeight: focusNum === maxPage ? "600" : "300",
          }}
        >
          {maxPage}
        </Num>
      </LastPage>
      <Arrow onClick={rightarrowClick}>
        <FontAwesomeIcon icon={faChevronRight} />
      </Arrow>
    </Wrap>
  );
};
