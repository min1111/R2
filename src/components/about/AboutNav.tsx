import styled from "styled-components";

const Wrap = styled.div`
  width: 100%;
  height: 45px;
  display: flex;
  align-items: center;
  margin-bottom: 50px;
  div {
    margin-right: 20px;
  }
`;
const Nav2 = styled.div`
  width: 70px;
  height: 100%;
  border-radius: 25px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: black;
  color: white;
  font-size: 18px;
  font-weight: 600;
  cursor: pointer;
`;
const Nav4 = styled.div`
  width: 100px;
  height: 100%;
  border-radius: 25px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 18px;
  background-color: white;
  cursor: pointer;
`;
const Nav5 = styled.div`
  width: 110px;
  height: 100%;
  border-radius: 25px;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 18px;
  background-color: white;
  cursor: pointer;
`;

interface style {
  background: string;
  color: string;
  fontWeight: string;
}

export const AboutNav = ({ click, focus }: any) => {
  const focusStyle: style = {
    background: "black",
    color: "white",
    fontWeight: "600",
  };
  const unfocusStyle: style = {
    background: "white",
    color: "black",
    fontWeight: "400",
  };

  return (
    <Wrap>
      <Nav2
        style={focus === "all" ? focusStyle : unfocusStyle}
        onClick={() => {
          click("all");
        }}
      >
        전체
      </Nav2>
      <Nav4
        style={focus === "report" ? focusStyle : unfocusStyle}
        onClick={() => {
          click("report");
        }}
      >
        보도자료
      </Nav4>
      <Nav5
        style={focus === "media" ? focusStyle : unfocusStyle}
        onClick={() => {
          click("media");
        }}
      >
        미디어행사
      </Nav5>
    </Wrap>
  );
};
