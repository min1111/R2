import styled from "styled-components";
import { AboutNav } from "./AboutNav";
import { AboutTitle } from "./AboutTitle";
import { useState, useEffect } from "react";
import { AboutNews } from "./AboutNews";
import { AboutPage } from "./AboutPage";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { changeBoard, enterBoard } from "../../_actions/board_action";
import { Loading } from "../Loading";
import { AboutPostButton } from "./AboutPostButton";

const Section = styled.section`
  padding: 120px 120px;
`;

export const About = () => {
  const dispath = useDispatch();
  const [loading, setLoading] = useState(false);
  const [focus, setFocus] = useState("all");
  const [page, setPage] = useState(1);
  const board: any = useSelector(
    (state: any) => state?.board?.board?.data?.results
  );

  console.log(board);
  const onClick = async (division: string) => {
    setLoading(false);
    setFocus(division);
    setPage(1);

    dispath(await changeBoard(division, 0)).then((res: any) => res);

    setLoading(true);
  };
  const onPageClick = async (num: number) => {
    setLoading(false);
    setPage(num);

    dispath(await changeBoard(focus, num - 1)).then((res: any) => res);

    setLoading(true);
  };

  useEffect(() => {
    const boardDb = async () => {
      await dispath(enterBoard()).then((res: any) => {
        setLoading(true);
      });
    };
    boardDb();
  }, []);

  return (
    <Section>
      <AboutTitle />
      <AboutNav click={onClick} focus={focus} />
      {loading ? <AboutNews array={board.content} /> : <Loading />}
      <AboutPostButton />
      {loading ? (
        <AboutPage
          focusNum={page}
          click={onPageClick}
          totalPage={board.totalPages}
        />
      ) : (
        <Loading />
      )}
    </Section>
  );
};
