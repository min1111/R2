import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styled from "styled-components";

const Section = styled.section``;
const Icon = styled.div`
  display: inline-block;
  font-size: 40px;
  color: #fdde1b;
`;
const Title = styled.h1`
  display: inline-block;
  font-size: 48px;
  font-weight: 500;
  margin-left: 20px;
  margin-bottom: 40px;
`;
const SubTitle = styled.div`
  font-size: 36px;
  font-weight: 500;
  margin-bottom: 100px;
`;
const SubTitle2 = styled.div`
  font-size: 36px;
  font-weight: 500;
  margin-bottom: 60px;
`;

export const AboutTitle = () => {
  return (
    <Section>
      <Icon>
        <FontAwesomeIcon icon={faPaperPlane} />
      </Icon>
      <Title>뉴스</Title>
      <SubTitle>가장 빠른 Pacom 업데이트 소식</SubTitle>
      <SubTitle2>뉴스 아카이브</SubTitle2>
    </Section>
  );
};
