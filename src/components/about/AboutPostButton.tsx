import { Link } from "react-router-dom";
import styled from "styled-components";

const Wrap = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: right;
  min-width: 1340px;
`;

const Button = styled.div`
  margin-top: 50px;
  width: 200px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: white;
  border-radius: 25px;
  font-size: 20px;
  font-weight: 600;
`;

export const AboutPostButton = () => {
  return (
    <Wrap>
      <Link to={"/aboutposting"}>
        <Button>글 쓰 기</Button>
      </Link>
    </Wrap>
  );
};
