import { Link, useNavigate } from "react-router-dom";
import styled from "styled-components";
import { authApi } from "../api";
import { useSelector } from "react-redux";
import { SockBell } from "./sock/SockBell";
import { MessengerHeader } from "./messenger/MessengerHeader";

const Sheader = styled.div`
  width: 100%;
  height: 80px;
  padding: 0 110px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 9998;
`;
const Logo = styled.div`
  a {
    font-size: 32px;
    font-weight: 900;
    color: #002d7d;
  }
`;
const MenuWrap = styled.ul`
  display: flex;
  align-content: center;
`;
const Menu = styled.li`
  margin-right: 100px;
  font-size: 20px;
  font-weight: 600;
`;
const RightMenuWrap = styled.div`
  display: flex;
  align-items: center;
  justify-content: right;
`;
const Logout = styled.div`
  width: 80px;
  height: 30px;
  background-color: black;
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 200;
  font-size: 14px;
  color: white;
  cursor: pointer;
  margin-left: 30px;
`;

export const Header = () => {
  const navigate = useNavigate();
  const user: any = useSelector(
    (state: any) => state?.user?.loginSuccess?.data
  );

  const logoutClick = () => {
    authApi()
      .get(`/logout`, {
        headers: {
          Authorization: `${
            localStorage.getItem("authorization")
              ? localStorage.getItem("authorization")
              : "Bearer"
          }`,
        },
      })
      .then((res) => {
        if (res.data.code !== -1) {
          console.log(res);
        } else {
          navigate("/");
        }
      });
  };
  return (
    <>
      {user?.code === 1 ? (
        <>
          <Sheader>
            <Logo>
              <Link to="/home">Pacom</Link>
            </Logo>
            <MenuWrap>
              <Menu>
                <Link to="/home">Home</Link>
              </Menu>
              <Menu>
                <Link to="/about">News</Link>
              </Menu>
              {user?.result?.role !== "ROLE_USER" ? (
                <Menu>
                  <Link to="/admin">Admin</Link>
                </Menu>
              ) : (
                ""
              )}
            </MenuWrap>
            <RightMenuWrap>
              <SockBell />
              <Logout onClick={logoutClick}>Logout</Logout>
            </RightMenuWrap>
          </Sheader>
          <MessengerHeader />
        </>
      ) : (
        ""
      )}
    </>
  );
};
