import React, { useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { About } from "./components/about/About";
import { AboutDetail } from "./components/aboutDetail/AboutDetail";
import { AboutPosting } from "./components/aboutPosting/AboutPosting";
import { Footer } from "./components/Footer";
import { Header } from "./components/Header";
import { Home } from "./components/home/Home";
import { Login } from "./components/login/Login";
import auth from "./hoc/auth";
import { GlobalStyle } from "./styles/GlobalStyle";
import { TableDashboard } from "./components/home/table/TableDashboard";
import { AdminPage } from "./components/adminPage/AdminPage";
import { MessengerRoom } from "./components/messenger/messengerRoom/MessengerRoom";
import { useSelector } from "react-redux";
import { sockstatus } from "./_actions/sock_action";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { Loading } from "./components/Loading";
import { AdminUserDetail } from "./components/adminPage/AdminUserDetail";

function App() {
  const AuthHome = auth(Home, true, false);
  const AuthLogin = auth(Login, false, false);
  const AuthAbout = auth(About, true, false);
  const AuthAboutDetail = auth(AboutDetail, true, false);
  const AuthAboutPosting = auth(AboutPosting, true, false);
  const Authadmin = auth(AdminPage, true, true);
  const AuthadminUserDetail = auth(AdminUserDetail, true, true);
  const AuthMessengerRoom = auth(MessengerRoom, true, false);
  const dispatch = useDispatch();

  return (
    <>
      <Router>
        <GlobalStyle />
        <Header />
        <Routes>
          <Route path="/" element={<AuthLogin />} />
          <Route path="/home" element={<AuthHome />} />
          <Route path="/about" element={<AuthAbout />} />
          <Route path="/aboutDetail/:id" element={<AuthAboutDetail />} />
          <Route path="/aboutPosting" element={<AuthAboutPosting />} />
          <Route path="/admin" element={<Authadmin />} />
          <Route
            path="/admin/user/detail/:id"
            element={<AuthadminUserDetail />}
          />
          <Route path="/messenger/:id" element={<AuthMessengerRoom />} />
        </Routes>
        <Footer />
      </Router>
    </>
  );
}

export default App;
