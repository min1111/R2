import { CHANGE_BOARD, ENTER_BOARD } from "../_actions/types";

export default function (state = {}, action: any) {
  switch (action.type) {
    case ENTER_BOARD:
      return { ...state, board: action.payload };
    case CHANGE_BOARD:
      return { ...state, board: action.payload };

    default:
      return state;
  }
}
