import { combineReducers } from "redux";
import user from "./user_reducer";
import board from "./board_reducer";
import sock from "./sock_reducer";
import hydrogen from "./hydrogen_reducer";

const rootReducer = combineReducers({
  user,
  board,
  sock,
  hydrogen,
});

export default rootReducer;

export type RootState = ReturnType<typeof rootReducer>;
