import { SOCKSTATUS_SOCK } from "../_actions/types";
import {
  CHATLIST_SOCK,
  CHATROOMLIST_SOCK,
  ENTERROOM_SOCK,
  PUSHMESSAGE_SOCK,
} from "../_actions/types";

export default function (state = {}, action: any) {
  switch (action.type) {
    case ENTERROOM_SOCK:
      return { ...state, enter: action.payload };
    case CHATLIST_SOCK:
      return { ...state, chatlist: action.payload };
    case CHATROOMLIST_SOCK:
      return { ...state, chatroomlist: action.payload };
    case PUSHMESSAGE_SOCK:
      return { ...state, pushmessage: action.payload };
    case SOCKSTATUS_SOCK:
      return { ...state, sockstatus: action.payload };

    default:
      return state;
  }
}
