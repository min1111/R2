import { HYDROGEN_SOCK } from "../_actions/types";

export default function (state = {}, action: any) {
  switch (action.type) {
    case HYDROGEN_SOCK:
      return { ...state, status: action.payload };

    default:
      return state;
  }
}
