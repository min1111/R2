import { authApi } from "../api";
import { setToken } from "../components/login/LoginInput";
import { CHANGE_BOARD, ENTER_BOARD } from "./types";

export const enterBoard: any = () => {
  const request = authApi()
    .get("/api/v1/user/boards/all", {
      headers: {
        Authorization: `${
          localStorage.getItem("authorization")
            ? localStorage.getItem("authorization")
            : "Bearer"
        }`,
      },
    })
    .then((res: any) => {
      setToken(res.headers.authorization);
      console.log(res);
      return res;
    });
  return {
    type: ENTER_BOARD,
    payload: request,
  };
};

export const changeBoard: any = (division: string, page: number) => {
  const request = authApi()
    .get(`/api/v1/user/boards/${division}?page=${page}`, {
      headers: {
        Authorization: `${
          localStorage.getItem("authorization")
            ? localStorage.getItem("authorization")
            : "Bearer"
        }`,
      },
    })
    .then((res: any) => {
      setToken(res.headers.authorization);
      return res;
    });
  return {
    type: CHANGE_BOARD,
    payload: request,
  };
};
