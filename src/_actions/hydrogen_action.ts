import { HYDROGEN_SOCK } from "./types";

export const hydrogenStatus: any = (dataToSubmit: any) => {
  const request = dataToSubmit;
  return {
    type: HYDROGEN_SOCK,
    payload: request,
  };
};
