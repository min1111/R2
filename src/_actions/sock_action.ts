import {
  CHATLIST_SOCK,
  CHATROOMLIST_SOCK,
  ENTERROOM_SOCK,
  PUSHMESSAGE_SOCK,
  SOCKSTATUS_SOCK,
} from "./types";

export const enterRoom: any = (dataToSubmit: any) => {
  const request = dataToSubmit.result;
  return {
    type: ENTERROOM_SOCK,
    payload: request,
  };
};

export const chatList: any = (dataToSubmit: any) => {
  const request = dataToSubmit;
  return {
    type: CHATLIST_SOCK,
    payload: request,
  };
};

export const chatRoomList: any = (dataToSubmit: any) => {
  const request = dataToSubmit;
  return {
    type: CHATROOMLIST_SOCK,
    payload: request,
  };
};

export const pushMessage: any = (dataToSubmit: any) => {
  const request = dataToSubmit;

  return {
    type: PUSHMESSAGE_SOCK,
    payload: request,
  };
};

export const sockstatus: any = (dataToSubmit: any) => {
  const request = dataToSubmit;
  return {
    type: SOCKSTATUS_SOCK,
    payload: request,
  };
};
